#include "orx.h"
#include "element.h"
#include "directionrolloverelement.h"
#include <sys/time.h>

directionrolloverelement::directionrolloverelement(int points, orxSTRING upDirectionEventName, orxSTRING downDirectionEventName) 
: element(points, (orxCHAR*)"DirectionRollOverObject", orxNULL) {
	activatorHitAnimationName = orxNULL;
	lightOnAnimationName = orxNULL;
	
	upEventName = upDirectionEventName;
	downEventName = downDirectionEventName;
}

directionrolloverelement::directionrolloverelement(int points, orxBOOL narrow, orxSTRING upDirectionEventName, orxSTRING downDirectionEventName) 
: element(points, (orxCHAR*)"DirectionRollOverCloserObject", orxNULL) {
	activatorHitAnimationName = orxNULL;
	lightOnAnimationName = orxNULL;
	
	upEventName = upDirectionEventName;
	downEventName = downDirectionEventName;
}

directionrolloverelement::~directionrolloverelement(){
}

void directionrolloverelement::RunRules() {
	
	orxLOG("orxSystem_GetTime () %f", orxSystem_GetTime () );
}

int directionrolloverelement::RegisterHit(orxSTRING bodyNameHit) {
	if (orxString_Compare(bodyNameHit, "DirectionRollOverBodyPart2") == 0 || orxString_Compare(bodyNameHit, "DirectionRollOverBodyPart2Closer") == 0){
		topBodyHitTime = orxSystem_GetTime();
		//orxLOG("topBodyHitTime %f", topBodyHitTime);
	}
	if (orxString_Compare(bodyNameHit, "DirectionRollOverBodyPart1") == 0){
		midBodyHitTime = orxSystem_GetTime();
		//orxLOG("midBodyHitTime %f", midBodyHitTime);
	}
	if (orxString_Compare(bodyNameHit, "DirectionRollOverBodyPart3") == 0 || orxString_Compare(bodyNameHit, "DirectionRollOverBodyPart3Closer") == 0){
		bottomBodyHitTime = orxSystem_GetTime();
		//orxLOG("bottomBodyHitTime %f", bottomBodyHitTime);
	}
	
	if (topBodyHitTime != orxNULL && midBodyHitTime != orxNULL && bottomBodyHitTime != orxNULL){
		if (topBodyHitTime > midBodyHitTime && midBodyHitTime > bottomBodyHitTime && upEventName != orxNULL){
			if ( (topBodyHitTime - midBodyHitTime) < 1 && (midBodyHitTime - bottomBodyHitTime) < 1){
				//orxLOG("up %s", upEventName);
				orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED, 1, orxNULL, orxNULL, upEventName);
			}
		}
		if (topBodyHitTime < midBodyHitTime && midBodyHitTime < bottomBodyHitTime&& downEventName != orxNULL){
			if ( (midBodyHitTime - topBodyHitTime) < 1 && (bottomBodyHitTime - midBodyHitTime) < 1){
				//orxLOG("down %s", downEventName);
				orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED, 1, orxNULL, orxNULL, downEventName);
			}
		}
	}
	
	return points;
}