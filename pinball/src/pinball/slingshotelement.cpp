#include "orx.h"
#include "element.h"
#include "slingshotelement.h"

/*slingshotleftelement::slingshotleftelement(int points) : element(points, (orxCHAR*)"SlingShotLeftObject", orxNULL) {
	activatorHitAnimationName = (orxCHAR*)"SlingShotLeftHitAnim";
	lightOnAnimationName = orxNULL;
}*/

slingshotelement::slingshotelement(int p, orxSTRING activatorNameFromConfig, orxSTRING activatorHitAnimationNameFromConfig) 
: element(p, activatorNameFromConfig, orxNULL){
	activatorHitAnimationName = activatorHitAnimationNameFromConfig;
	lightOnAnimationName = orxNULL;
}


slingshotelement::~slingshotelement() {

}

