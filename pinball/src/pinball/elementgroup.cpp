#include "orx.h"
#include "elementgroup.h"
#include "element.h"
#include "targetleftelement.h"
#include <vector>

elementgroup::elementgroup()
{
	eventName = (orxCHAR*)"GROUP_FILLED";
}

elementgroup::~elementgroup()
{
}

void elementgroup::Add(element *e) {
	elements.push_back(e);
	
	e->SetElementParentGroup(this);
}

int elementgroup::RunGroupRules(){
	int pointsToReturn = 0;
	
	//if all lights are on, flash then and let them go off.
	if (LightsOnCount() == elements.size()){
		FlashAllLights();
		DoSpanglesOnLights();
		orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED, 1, orxNULL, orxNULL, eventName);
		return 25000;
	}
	
	//if all activators are on, turn them all off.
	if (ActivatorsOnCount() == elements.size()){
		//TurnAllActivatorsOff();
		FlashAllActivators(); //need to work out how to test types in the vector in order to flash one or the other.
		DoSpanglesOnActivators();
		orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED, 1, orxNULL, orxNULL, eventName);
		return 25000;
	}
	
	return 0;
}

void elementgroup::SetEventName(orxSTRING name){
	eventName = name;
	
}


void elementgroup::TurnAllLightsOff(){
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		e->TurnOffLight();
	}
}

void elementgroup::FlashAllLights(){
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		e->FlashLight();
	}
}

void elementgroup::FlashAllActivators(){
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		e->FlashActivator();
	}
}

int elementgroup::LightsOnCount(){
	int elementsLit = 0; 
	
	//check how many lights are on.
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		
		if (e->IsLit() == orxTRUE){
			elementsLit++;
		}
	}
	return elementsLit;
}

int elementgroup::ActivatorsOnCount(){
	int activatorsLit = 0; 
	
	//check how many lights are on.
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		
		if (e->IsActivatorOn() == orxTRUE){
			activatorsLit++;
		}
	}
	return activatorsLit;
}

void elementgroup::TurnAllActivatorsOff(){
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		e->TurnOffActivator();
	}
}

void elementgroup::TurnActivatorOnAt(int index){
	int size = elements.size();
	if (index <= size){
		element *e = elements[index];
		e->TurnOnActivator();
	}
}

void elementgroup::TurnLightOnAt(int index){
	int size = elements.size();
	if (index <= size){
		element *e = elements[index];
		e->TurnOnLight();
	}
}

void elementgroup::ShiftActivatorsRight(){
	std::vector<orxBOOL> switches;
	
	//load activator on/off states into the array
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		switches.push_back(e->IsActivatorOn());
	}
	
	//shift the activator on/off states
	orxBOOL last = switches.back();
	switches.pop_back();
	switches.insert(switches.begin(), last);
	
	//reapply on/off states back to the elements
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		orxBOOL onOff = switches[x];
		if (onOff == orxTRUE){
			e->TurnOnActivator();
		} else {
			e->TurnOffActivator();
		}
	}
}

void elementgroup::ShiftLightsRight(){
	std::vector<orxBOOL> switches;
	
	//load light on/off states into the array
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		switches.push_back(e->IsLit());
	}
	
	//shift the activator on/off states
	orxBOOL last = switches.back();
	switches.pop_back();
	switches.insert(switches.begin(), last);
	
	//reapply on/off states back to the elements
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		orxBOOL onOff = switches[x];
		if (onOff == orxTRUE){
			e->TurnOnLight();
		} else {
			e->TurnOffLight();
		}
	}
}

void elementgroup::DoSpanglesOnActivators(){
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		e->CreateSpanglesOnActivator((orxCHAR*)"MainParticleObject");
	}
}

void elementgroup::DoSpanglesOnLights(){
	for(int x=0; x<elements.size(); x++){
		element *e = elements[x];
		e->CreateSpanglesOnLight((orxCHAR*)"MainParticleObject");
	}
}

