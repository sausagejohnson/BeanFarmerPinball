#ifndef TARGETLEFTELEMENT_H
#define TARGETLEFTELEMENT_H

class targetleftelement : public element::element
{
public:
	targetleftelement(int points);
	~targetleftelement();
	
	void RunRules();
};

#endif // TARGETLEFTELEMENT_H
