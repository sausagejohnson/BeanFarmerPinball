/* Bean Farmer Pinball Pre-release
 *
 * Copyright (c) 2014 waynejohnson.net
 *
 * This software is licensed under the 
 * Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * http://creativecommons.org/licenses/by-nc-sa/4.0/
 * 
 * While the license does not allow commercial use, I permit the 
 * use (but not sale) of the software commerically.
 * This license will change over time to fit more appropriately.
 * 
 */

/**
 * @date started July 2014, finished December 2014
 * @author sausage@zeta.org.au
 *
 */

#include "orx.h"
#include "pinball.h"
#include "elementgroup.h"
#include "element.h"
#include "bumperelement.h"
#include "slingshotelement.h"
#include "targetleftelement.h"
#include "targetrightelement.h"
#include "targetrightelement.h"
#include "rolloverlightelement.h"
#include "directionrolloverelement.h"
#include "plunger.h"
#include <sstream>
#include <vector>
#include <winver.h>

orxBOOL leftFlipperPressed;
orxBOOL rightFlipperPressed;
orxBOOL launcherTrapOn;
orxBOOL leftTrapOn;
orxBOOL rightTrapOn;
orxBOOL ballInPlay;
orxBOOL launchMultiBalls;

/* When all three are filled, launch multiball */
orxBOOL rollOversFilled;
orxBOOL targetGroupLeftFilled;
orxBOOL targetGroupRightFilled;

orxBOOL isAndroid;

orxVIEWPORT *pstViewport;
orxCAMERA 	*pstCamera;
orxCLOCK    *pstDancingLightsClock;

orxOBJECT *tableObject;
orxOBJECT *ballObject;
orxOBJECT *leftFlipperObject;
orxOBJECT *rightFlipperObject;
orxOBJECT *ballCatcherObject;
orxOBJECT *scoreObject;
orxOBJECT *ballCountObject;
orxOBJECT *beanPoleObject;
orxOBJECT *x2Object;
orxOBJECT *x3Object;
orxOBJECT *x4Object;
orxOBJECT *x5Object;
orxOBJECT *x6Object;
orxOBJECT *trapObject;
orxOBJECT *trapLeftObject;
orxOBJECT *trapRightObject;
orxOBJECT *flukeLeftObject;
orxOBJECT *flukeRightObject;
orxOBJECT *leftTouchObject;
orxOBJECT *rightTouchObject;
orxOBJECT *swipeTouchObject;
orxOBJECT *titleObject;
orxOBJECT *loadingPageObject;
orxOBJECT *loadingProgressObject;

orxVECTOR touchDownVector = {0, 0, 0}; //The location where a touch occurs.
orxVECTOR touchUpVector = {0, 0, 0}; //The location where a touch is lifted.

elementgroup *targetGroupLeft;
elementgroup *targetGroupRight;

elementgroup *rolloverlightgroup;

plunger *plung;

const int SLINGSHOT_VARIANCE = 200;
const int PLUNGER_SMACK_VARIANCE = 150;
const float DRAG_DISTANCE_MAX = 310;
float TOUCH_WIDTH = 140;
float TOUCH_HEIGHT = 140;
float TOUCH_LEFT_X = 41;
float TOUCH_Y = 553;
float TOUCH_RIGHT_X = 378;

orxFLOAT nativeScreenWidth = 0;
orxFLOAT nativeScreenHeight = 0;

std::string scorePadding = "000,000,000";
orxU64 score = 0;
int balls = 0;
int multiBalls = 0;
std::vector<int> ballIncreases;

/* Delayed function parameters */
const int trapMaxTime = 10;
int trapTimer = 0;
int showTitleDelay = -1;
int zeroOutScoreDelay = -1;

const int TEXTURE_TO_LOAD_MAX = 34; //34 textures to expect
int texturesToLoadCount = 0;
orxBOOL endTextureLoad = orxFALSE;

/* Occurs every quarter second and it used to dance the lights during game over. */
void orxFASTCALL DancingLightsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	orxLOG("============Dancing light +");
	if (rolloverlightgroup->ActivatorsOnCount() == 0)
		rolloverlightgroup->TurnActivatorOnAt(0); //turn on first to get things cycling
	if (targetGroupLeft->LightsOnCount() == 0)
		targetGroupLeft->TurnLightOnAt(0); 
	if (targetGroupRight->LightsOnCount() == 0)
		targetGroupRight->TurnLightOnAt(0); 
		
	rolloverlightgroup->ShiftActivatorsRight();
	targetGroupLeft->ShiftLightsRight();
	targetGroupRight->ShiftLightsRight();
	if (zeroOutScoreDelay <= 0){
		if (score == 0){
			score = 8;
		}
		if (score >= 999999999){
			score = GetSavedHighScore();
			zeroOutScoreDelay = 5;
		}
		PrintScore();
		score *= 10;
	}
	
	int m = GetMultiplier();
	
	if (m >= 6 ){
		TurnOffAllMultiplierLights();
		return;
	} 
	IncreaseMultiplier();
	
	orxLOG("============Dancing light -");
	
}

/* Occurs every second and works for delayed or periodic running of tasks. */
void orxFASTCALL SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	orxLOG("============SecondsUpdate +");

	if (trapTimer > 0){
		trapTimer--;
		//orxLOG("Trap countdown: %d", trapTimer);
	} else { //if (trapTimer == 1)
		SetLeftTrap(orxFALSE);
		SetRightTrap(orxFALSE);
		trapTimer = 0;
	}
	
	/* for showing title at the end of a game */
	if (showTitleDelay > 0 ){
		showTitleDelay--;
		//orxLOG("showTitleDelay countdown: %d", showTitleDelay);
	} else if (showTitleDelay == 0) { 
		showTitleDelay = -1;
		ShowTitle();
		TurnOffAllTableLights();
		orxClock_Restart(pstDancingLightsClock);
		orxClock_Unpause(pstDancingLightsClock);
	}
	
	/* for when to zero out the score at the end of a game */
	if (zeroOutScoreDelay > 0 ){
		zeroOutScoreDelay--;
		//orxLOG("showTitleDelay countdown: %d", showTitleDelay);
	} else if (zeroOutScoreDelay == 0) { 
		zeroOutScoreDelay = -1;
		score = 0;
	}
	
	/* Multiball mode. Keep trying to create and launch balls until there are 5. */
	if (launchMultiBalls == orxTRUE){
		CreateBall();
		plung->SetPlungerStrength(50);
		LaunchBall();
		orxLOG("How many multiballs %d", multiBalls);
		if (multiBalls > 3){
			launchMultiBalls = orxFALSE;
		}
	}
	
	/* After multiballs all loaded, any extras left in the channel need to be launched */
	if (launchMultiBalls == orxFALSE && multiBalls > 0 ){
		orxOBJECT *ballLeftBehind = GetABallObjectAtThePlunger();
		if (ballLeftBehind != orxNULL){
			plung->SetPlungerStrength(50);
			LaunchBall();
		}
	}
	
	orxLOG("============SecondsUpdate -");
	
}

void ShowTitle(){
	orxLOG("ShowTitle +");
	orxLOG("titleObject +");
	
	titleObject = orxObject_CreateFromConfig("TitleObject");
	orxLOG("titleObject -");
	
	if (isAndroid){
		orxObject_AddTimeLineTrack(titleObject, "TitleTrackMobile");
	} else {
		orxObject_AddTimeLineTrack(titleObject, "TitleTrackDesktop");
	}
	orxObject_AddTimeLineTrack(titleObject, "ThemeTrack");
	
	orxLOG("ShowTitle -");
}

void HideTitle(){
	
	orxObject_SetLifeTime(titleObject, orxFLOAT_0);

}

void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig){
	if (object == orxNULL){
		orxLOG("For some reason, this object is null.");
		return;
	}
	orxObject_AddSound(object, soundFromConfig);
}

orxSTATUS orxFASTCALL CustomEventHandler(const orxEVENT *_pstEvent) {
	orxSTRING eventName = (orxCHAR*)_pstEvent->pstPayload;
	
	if (orxString_Compare(eventName, "BALL_LAUNCHED") == 0){
		SetLauncherTrap(orxTRUE);
		SetLeftTrap(orxTRUE);
		SetRightTrap(orxTRUE);
		trapTimer = trapMaxTime * GetMultiplier();
		PlaySoundOn(ballObject, (orxCHAR*)"LaunchTrapCloseEffect");
	}
	
	if (orxString_Compare(eventName, "LEFT_FLUKE_SHOT") == 0){
		AddToScoreAndUpdate(250000);
		FlashLeftFlukeLight();
		CreateSpanglesAtObject(flukeLeftObject, (orxCHAR*)"MainParticleObject");
		PlaySoundOn(flukeLeftObject, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "RIGHT_FLUKE_SHOT") == 0){
		AddToScoreAndUpdate(250000);
		FlashRightFlukeLight();
		CreateSpanglesAtObject(flukeRightObject, (orxCHAR*)"MainParticleObject");
		PlaySoundOn(flukeRightObject, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "TARGET_LEFT_GROUP_FILLED") == 0 ||
		orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0 ||
		orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0){
		IncreaseMultiplier();
		SetLeftTrap(orxTRUE);
		SetRightTrap(orxTRUE);
		trapTimer = trapMaxTime * GetMultiplier();
		PlaySoundOn(tableObject, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "TARGET_LEFT_GROUP_FILLED") == 0)
		targetGroupLeftFilled = orxTRUE;
	
	if (orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0)
		targetGroupRightFilled = orxTRUE;
		
	if (orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0)
		rollOversFilled = orxTRUE;
		
	if (targetGroupLeftFilled == orxTRUE && targetGroupRightFilled == orxTRUE && rollOversFilled == orxTRUE){
		targetGroupLeftFilled = orxFALSE;
		targetGroupRightFilled = orxFALSE;
		rollOversFilled = orxFALSE;
		launchMultiBalls = orxTRUE;
	}
	
}



void DestroyBallAndCreateNewBall(orxOBJECT *ballObject){
	DestroyBall(ballObject);
	
	launchMultiBalls = orxFALSE; //lose a ball during multiball launches and they turn off.
	
	if (multiBalls > 0){
		multiBalls--;
	}
	
	orxLOG("Multiballs: %d, balls %d", multiBalls, balls);
	
	if (IsGameOver() == orxTRUE){
		TryUpdateHighscore();
		showTitleDelay = 1;
		zeroOutScoreDelay = 5;
	}
	if (multiBalls == 0 && ballInPlay == orxFALSE){
		CreateBall();	
	}
}


orxSTATUS orxFASTCALL TextureLoadingEventHandler(const orxEVENT *_pstEvent) {
	if (endTextureLoad == orxTRUE){
		return orxSTATUS_SUCCESS;
	}
	

	
	if (_pstEvent->eType == orxEVENT_TYPE_TEXTURE){
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_CREATE){
			orxLOG("orxTEXTURE_EVENT_CREATE");
		}
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_NUMBER){
			orxLOG("orxTEXTURE_EVENT_NUMBER");
		}	
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_LOAD){
			//orxTIMELINE_EVENT_PAYLOAD *pstPayload;
			//pstPayload = (orxTIMELINE_EVENT_PAYLOAD *)_pstEvent->pstPayload;
			orxLOG("orxTEXTURE_EVENT_LOAD");
			
			orxTEXTURE *pstSenderObject;
			pstSenderObject = orxTEXTURE(_pstEvent->hSender);
			
			const orxSTRING name = orxTexture_GetName(pstSenderObject);
			
			texturesToLoadCount++;
			orxFLOAT percent = (orxFLOAT)texturesToLoadCount / (orxFLOAT)TEXTURE_TO_LOAD_MAX * 100;
			SetProgressPercent(percent);
			orxLOG("Texture %s loaded percent %f, textures to load %d", name, percent, texturesToLoadCount);
			//orxLOG("==== Texture %s %s ", pstPayload->zEvent, pstPayload->zTrackName);
			
			if (percent >= 100 && endTextureLoad == orxFALSE){
				endTextureLoad = orxTRUE;
				
				orxLOG("100 percent loaded. Time to Fade out.");
				
				orxObject_AddFX(loadingProgressObject, "LoadingPageFadeOutFXSlot");	
				orxObject_AddFX(loadingPageObject, "LoadingPageFadeOutFXSlot");	
				orxOBJECT *decal = orxOBJECT(orxObject_GetChild(loadingPageObject));
				orxObject_AddFX(decal, "LoadingPageFadeOutFXSlot");	
				
				orxObject_SetLifeTime(loadingProgressObject, 2); 
				orxObject_SetLifeTime(decal, 2); 
				orxObject_SetLifeTime(loadingPageObject, 2); 
				
			}
		}
	}
	
}

/** Input, Physics and Anim Event handler (TODO: break up)
 */
orxSTATUS orxFASTCALL EventHandler(const orxEVENT *_pstEvent) {
  
	if (_pstEvent->eType == orxEVENT_TYPE_VIEWPORT){
		
		if (_pstEvent->eID == orxVIEWPORT_EVENT_RESIZE){
			SetFrustumToWhateverDisplaySizeCurrentlyIs();
			
			orxFLOAT deviceWidth = 865;
			orxFLOAT deviceHeight = 1280;
			orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
			if (getSizeSuccess == orxTRUE){
				SetupTouchZonesValues(deviceWidth, deviceHeight);
				SetTouchHighlightPositions(deviceWidth, deviceHeight);
			}
		}
		
	}
	
	if (_pstEvent->eType == orxEVENT_TYPE_OBJECT){
		
		if (_pstEvent->eID == orxOBJECT_EVENT_DELETE){
			orxOBJECT *pstSenderObject;
			pstSenderObject = orxOBJECT(_pstEvent->hSender);
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "TitleObject") == 0){
				titleObject = orxNULL; //null so that if HideTitle() is called too quickly, titleObject may not dispose in time.
			}
			
		}
		
	}
	
	 	
	
  	if (_pstEvent->eType == orxEVENT_TYPE_INPUT){
	
		if (_pstEvent->eID == orxINPUT_EVENT_ON){
	
			if (orxMouse_IsButtonPressed(orxMOUSE_BUTTON_LEFT)){
				orxVECTOR mouseWorldPosition;
				
				//orxLOG("Mouse position: %f x %f", mouseWorldPosition.fX, mouseWorldPosition.fY);
				
				orxObject_Enable(rightTouchObject, orxFALSE);
				orxOBJECT *object = orxObject_Pick(&mouseWorldPosition, orxU32_UNDEFINED);
				
				if (object != orxNULL){
					const orxSTRING objectName;
					objectName = orxObject_GetName(object);
					orxLOG("Clicked on object: %s x:%f y:%f z:%f", objectName, mouseWorldPosition.fX, mouseWorldPosition.fY, mouseWorldPosition.fZ);
				}

				
				orxObject_Enable(rightTouchObject, orxTRUE);
				
			}
			if (orxInput_IsActive("LeftFlipper") && orxInput_HasNewStatus("LeftFlipper")){
				if (IsGameOver() == orxFALSE){
					leftFlipperPressed = orxTRUE;
					StrikeLeftFlipper();
				}
			}
			if (orxInput_IsActive("RightFlipper") && orxInput_HasNewStatus("RightFlipper")){
				if (IsGameOver() == orxFALSE){
					rightFlipperPressed = orxTRUE;
					StrikeRightFlipper();
				}
			}
			if (orxInput_IsActive("PlayGame") == orxTRUE && orxInput_HasNewStatus("PlayGame") && orxInput_IsActive("Alt") == orxFALSE){
				orxLOG("orxInput_IsActive(PlayGame) +");
				StartNewGame();
				orxLOG("orxInput_IsActive(PlayGame) -");
			}	
			// Full screen switch
			if (	
			orxInput_IsActive("PlayGame") == orxTRUE && orxInput_HasNewStatus("PlayGame") &&
					orxInput_IsActive("Alt") == orxTRUE //&& orxInput_HasNewStatus("Alt")
			){
				orxBOOL isFullScreen = orxDisplay_IsFullScreen();
				orxDisplay_SetFullScreen (!isFullScreen);
			
				orxLOG("FULLSCREEN!!");
			}	
			if (orxInput_IsActive("PlungerSmack") && orxInput_HasNewStatus("PlungerSmack")){
				plung->SetPlungerStrength(53);
				LaunchBall();
			}

			if (orxInput_IsActive("TestKey") == orxTRUE && orxInput_HasNewStatus("TestKey")){
			}
			if (orxInput_IsActive("TestKey2") == orxTRUE && orxInput_HasNewStatus("TestKey2")){
			}
			if (orxInput_IsActive("PlungerPull") == orxTRUE && orxInput_HasNewStatus("PlungerPull")){
				orxLOG("--------------- PlungerPull KEY ON ------------------");
				plung->plungerIsBeingPulled = orxTRUE;
			}

		}
		
		if (_pstEvent->eID == orxINPUT_EVENT_OFF){
			if (orxInput_IsActive("LeftFlipper") == orxFALSE && orxInput_HasNewStatus("LeftFlipper")){
				PlaySoundOn(leftFlipperObject, (orxCHAR*)"FlipperDownEffect");
				leftFlipperPressed = orxFALSE;
			}
			if (orxInput_IsActive("RightFlipper") == orxFALSE && orxInput_HasNewStatus("RightFlipper")){
				PlaySoundOn(rightFlipperObject, (orxCHAR*)"FlipperDownEffect");
				rightFlipperPressed = orxFALSE;
			}
			if (orxInput_IsActive("Load") == orxFALSE && orxInput_HasNewStatus("Load")){
				CreateBall();
			}
			if (orxInput_IsActive("TestKey") == orxFALSE && orxInput_HasNewStatus("TestKey")){
			}
			if (orxInput_IsActive("PlungerSmack") == orxFALSE && orxInput_HasNewStatus("PlungerSmack")){
				plung->RestorePosition();
			}
			if (orxInput_IsActive("PlungerPull") == orxFALSE && orxInput_HasNewStatus("PlungerPull")){
				//orxLOG("--------------- PlungerPull KEY OFF ------------------");
				LaunchBall();
				plung->plungerIsBeingPulled = orxFALSE;
			}
		}
		
	}
	
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS){
	
		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;
		
		if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD)
		{
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BumperObject") == 0 
			){
				orxVECTOR hitVector = pstPayload->vNormal;
				ProcessBallAndBumperCollision(pstSenderObject, pstRecipientObject, hitVector);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"MainParticleObject");				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BumperObject") == 0 
			){
				orxVECTOR hitVector = FlipVector(pstPayload->vNormal);
				ProcessBallAndBumperCollision(pstRecipientObject, pstSenderObject, hitVector);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"MainParticleObject");
			} 		

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), (orxCHAR*)"SlingShotLeftObject") == 0 
			){
				ProcessBallAndLeftSlingShotCollision(pstSenderObject, pstRecipientObject);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"LighterMainParticleObject");				
				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SlingShotLeftObject") == 0 
			){
				ProcessBallAndLeftSlingShotCollision(pstRecipientObject, pstSenderObject);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"LighterMainParticleObject");				
			} 	
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "SlingShotRightObject") == 0 
			){
				ProcessBallAndRightSlingShotCollision(pstSenderObject, pstRecipientObject);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"LighterMainParticleObject");				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SlingShotRightObject") == 0 
			){
				ProcessBallAndRightSlingShotCollision(pstRecipientObject, pstSenderObject);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"LighterMainParticleObject");				
			} 
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TargetLeftObject") == 0 
			){
				ProcessBallAndTargetLeftCollision(pstSenderObject, pstRecipientObject);
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TargetLeftObject") == 0 
			){
				ProcessBallAndTargetLeftCollision(pstRecipientObject, pstSenderObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TargetRightObject") == 0 
			){
				ProcessBallAndTargetRightCollision(pstSenderObject, pstRecipientObject);
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TargetRightObject") == 0 
			){
				ProcessBallAndTargetRightCollision(pstRecipientObject, pstSenderObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BallCatcherObject") == 0 
			){
				DestroyBallAndCreateNewBall(pstRecipientObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BallCatcherObject") == 0 
			){
				DestroyBallAndCreateNewBall(pstSenderObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "RolloverLightObject") == 0 
			){
				ProcessBallAndRolloverLightCollision(pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "RolloverLightObject") == 0 
			){
				ProcessBallAndRolloverLightCollision(pstRecipientObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				(orxString_Compare(orxObject_GetName(pstSenderObject), "DirectionRollOverObject") == 0 ||
				orxString_Compare(orxObject_GetName(pstSenderObject), "DirectionRollOverCloserObject") == 0 )
			){
				ProcessBallAndDirectionRolloverCollision(pstSenderObject, (orxSTRING)pstPayload->zSenderPartName);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				(orxString_Compare(orxObject_GetName(pstRecipientObject), "DirectionRollOverObject") == 0 ||
				orxString_Compare(orxObject_GetName(pstRecipientObject), "DirectionRollOverCloserObject") == 0  )
			){
				ProcessBallAndDirectionRolloverCollision(pstRecipientObject, (orxSTRING)pstPayload->zRecipientPartName);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TableObject") == 0 
			){
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"MainDustParticle");
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TableObject") == 0 
			){
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"MainDustParticle");
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "FlukeLeftObject") == 0 
			){
				SetLeftTrap(orxFALSE);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "FlukeLeftObject") == 0 
			){
				SetLeftTrap(orxFALSE);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "FlukeRightObject") == 0 
			){
				SetRightTrap(orxFALSE);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "FlukeRightObject") == 0 
			){
				SetRightTrap(orxFALSE);
			} 
			
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TrapObject") == 0 
			){
				if (GetABallObjectIntheChannel() != orxNULL){
					SetLauncherTrap(orxFALSE);
				}
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TrapObject") == 0 
			){
				if (GetABallObjectIntheChannel() != orxNULL){
					SetLauncherTrap(orxFALSE);
				}
			} 
		
		}
	}
	
	
	
	
	if (_pstEvent->eType == orxEVENT_TYPE_SYSTEM){


		
		if(_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_BEGIN ){
			//orxLOG("------------ TOUCH BEGIN");
			orxSYSTEM_EVENT_PAYLOAD *payload;
			payload = (orxSYSTEM_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			
			/*Single Tap anywhere will restart the game if appropriate*/
			orxLOG("balls: %d", balls);
			orxLOG("ballinPlay: %d", ballInPlay);
			orxLOG("IsGameOver: %d", IsGameOver());
			orxLOG("IsTitleVisible: %d", IsTitleVisible());
			if (IsGameOver() == orxTRUE && IsTitleVisible() == orxTRUE){
				orxLOG("_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_BEGIN +");
				StartNewGame();
				orxLOG("_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_BEGIN -");
				//return orxSTATUS_SUCCESS; //check that returning here doesn't require clearing previous touch flags
			}
			

			
			orxVECTOR localTouchVector = { 0,0,0 };
			localTouchVector.fX = payload->stTouch.fX;
			localTouchVector.fY = payload->stTouch.fY;	

			orxVECTOR touchWorldPosition;
			touchWorldPosition = GetAdjustedTouchCoords(localTouchVector);
			
			touchDownVector = localTouchVector;
			
			orxBOOL leftTouched = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, TOUCH_LEFT_X, TOUCH_Y, TOUCH_LEFT_X + TOUCH_WIDTH, TOUCH_HEIGHT *2);
			orxBOOL rightTouched = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, TOUCH_RIGHT_X, TOUCH_Y, TOUCH_RIGHT_X + TOUCH_WIDTH, TOUCH_HEIGHT *2);
			orxBOOL swipeTouched = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, 174, 139, 284, 557);
			
			if (leftTouched == orxTRUE){
				orxObject_RemoveFX(leftTouchObject , "TouchFadeOutFXSlot");
				orxObject_AddFX(leftTouchObject , "TouchFadeUpFXSlot");
				leftFlipperPressed = orxTRUE;
				StrikeLeftFlipper();
			}
			
			if (rightTouched == orxTRUE){
				orxObject_RemoveFX(rightTouchObject , "TouchFadeOutFXSlot");
				orxObject_AddFX(rightTouchObject , "TouchFadeUpFXSlot");
				rightFlipperPressed = orxTRUE;
				StrikeRightFlipper();
			}
			
			if (swipeTouched == orxTRUE){
			}
			
			
		} else if(_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_END  ){
			
			
			//orxLOG("------------ TOUCH END ---------------");
			
			
			orxSYSTEM_EVENT_PAYLOAD *payload;
			payload = (orxSYSTEM_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			//orxLOG("Location %f x %f", payload->stTouch.fX, payload->stTouch.fY);
			
			//orxLOG("Mouse up");	

			orxVECTOR localTouchVector = { 0,0,0 };
			localTouchVector.fX = payload->stTouch.fX;
			localTouchVector.fY = payload->stTouch.fY;	

			orxVECTOR touchWorldPosition;
			touchWorldPosition = GetAdjustedTouchCoords(localTouchVector);

			touchUpVector = 	localTouchVector;
			
			orxBOOL leftUnTouched = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, TOUCH_LEFT_X, TOUCH_Y, TOUCH_LEFT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);
			orxBOOL rightUnTouched = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, TOUCH_RIGHT_X, TOUCH_Y, TOUCH_RIGHT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);
			orxBOOL swipeUnTouched = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, 174, 139, 284, 557);
			
			if (leftUnTouched == orxTRUE){
				orxObject_RemoveFX(leftTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(leftTouchObject , "TouchFadeOutFXSlot");
				leftFlipperPressed = orxFALSE;
			}
			
			if (rightUnTouched == orxTRUE){
				orxObject_RemoveFX(rightTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(rightTouchObject , "TouchFadeOutFXSlot");
				rightFlipperPressed = orxFALSE;
			}
			
			if (swipeUnTouched == orxTRUE){
				LaunchBall();
			} 
			else {
				LaunchBall();
			}
			
			orxFLOAT dragDistance = orxVector_GetDistance(&touchDownVector, &touchUpVector);
			//orxLOG("Drag distance: %f", dragDistance);
			if (dragDistance > 10 && dragDistance < DRAG_DISTANCE_MAX){
				
			}

			
		} else if(_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_MOVE  ){
			
			//orxLOG("--- --- orxSYSTEM_EVENT_TOUCH_MOVE");
			orxSYSTEM_EVENT_PAYLOAD *payload;
			payload = (orxSYSTEM_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			
			orxVECTOR localTouchVector = { 0,0,0 };
			localTouchVector.fX = payload->stTouch.fX;
			localTouchVector.fY = payload->stTouch.fY;	
			
			orxVECTOR touchWorldPosition;
			touchWorldPosition = GetAdjustedTouchCoords(localTouchVector);
			
			orxBOOL leftMovingWithin = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, TOUCH_LEFT_X, TOUCH_Y, TOUCH_LEFT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);
			orxBOOL rightMovingWithin = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, TOUCH_RIGHT_X, TOUCH_Y, TOUCH_RIGHT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);
			orxBOOL swipeMovingWithin = IsWithin(touchWorldPosition.fX, touchWorldPosition.fY, 174, 139, 284, 557);
			
			/*this fixed moving out of the zone but turns off the other flipper if a new one is pressed, so commenting out...
			if (leftMovingWithin == orxFALSE && leftFlipperPressed == orxTRUE){
				orxObject_RemoveFX(leftTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(leftTouchObject , "TouchFadeOutFXSlot");
				leftFlipperPressed = orxFALSE;
				orxLOG("-----------TOUCH LEFT OFF");
			}
			
			if (rightMovingWithin == orxFALSE && rightFlipperPressed == orxTRUE){
				orxObject_RemoveFX(rightTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(rightTouchObject , "TouchFadeOutFXSlot");
				rightFlipperPressed = orxFALSE;
			}
			//end of commenting
			*/
			
/*			if (swipeMovingWithin == orxFALSE){
				orxCOLOR touchColour;
				orxObject_GetColor(swipeTouchObject, &touchColour);
				if (touchColour.fAlpha == 1){
					orxObject_RemoveFX(swipeTouchObject , "TouchFadeUpFXSlot");
					orxObject_AddFX(swipeTouchObject , "TouchFadeOutFXSlot");
				}
			}*/
			
			orxFLOAT dragDistance = orxVector_GetDistance(&touchDownVector, &localTouchVector);
			//orxLOG("Move Drag distance: %f", dragDistance);
			if (dragDistance > 10 && dragDistance < DRAG_DISTANCE_MAX){
				float percent = (dragDistance / DRAG_DISTANCE_MAX) * 100;
				plung->SetPlungerLevelByPercent(percent);
				plung->SetPlungerStrengthByPercent(percent);
				//orxLOG("SetPlungerLevelByPercent %f", percent);
			}
		}
	}
	
	
	/* Done! */
	return orxSTATUS_SUCCESS;
}

orxBOOL IsTitleVisible(){
	return (titleObject != orxNULL);
}

orxBOOL IsGameOver(){
	if (balls == 0 && multiBalls == 0 && ballInPlay == orxFALSE){
		return orxTRUE;
	} 
	
	return orxFALSE;
}

/* a bug to have to do this???? */
orxVECTOR GetAdjustedTouchCoords(orxVECTOR touchInVector){
	return touchInVector;
	orxVECTOR adjustedTouchWorldPosition;
	orxRender_GetWorldPosition( &touchInVector, orxNULL, &adjustedTouchWorldPosition );

	//adjustedTouchWorldPosition.fY += 162;
	adjustedTouchWorldPosition.fZ = 0;
	return adjustedTouchWorldPosition;
}

void TurnOffAllTableLights(){
	orxLOG("TurnOffAllTableLights +");
	rolloverlightgroup->TurnAllActivatorsOff();
	rolloverlightgroup->TurnAllLightsOff();
	targetGroupLeft->TurnAllActivatorsOff();
	targetGroupLeft->TurnAllLightsOff();
	targetGroupRight->TurnAllActivatorsOff();
	targetGroupRight->TurnAllLightsOff();
	orxLOG("TurnOffAllTableLights -");
}

//only works if no balls and nothing in play
void StartNewGame(){
	orxLOG("StartNewGame +");
	if (IsGameOver() == orxTRUE){
		HideTitle();
		orxClock_Pause(pstDancingLightsClock);
	
		orxLOG("New game logged");
		score = 0;
		PrintScore();
		balls = 5;
		PrintBallCount();
		
		TurnOffAllTableLights();
		TurnOffAllMultiplierLights();
		
		CreateBall();
	}
	orxLOG("StartNewGame -");
}

void StrikeLeftFlipper(){
	orxObject_SetAngularVelocity(leftFlipperObject, -20.8);
	PlaySoundOn(leftFlipperObject, (orxCHAR*)"FlipperEffect");
}
void StrikeRightFlipper(){
	rolloverlightgroup->ShiftActivatorsRight();
	orxObject_SetAngularVelocity(rightFlipperObject, 20.8);
	PlaySoundOn(rightFlipperObject, (orxCHAR*)"FlipperEffect");
}

orxBOOL IsWithin(float x, float y, float x1, float y1, float x2, float y2){
	if (x >= x1 && x <= x2){
		if (y >= y1 && y <= y2){
			return orxTRUE;
		}
	}
	return orxFALSE;
}


void CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig){

	if (object == orxNULL)
		return;
		
	orxVECTOR objectVector;
	orxObject_GetPosition(object, &objectVector);
	objectVector.fZ = -0.1;
	
	orxOBJECT *splash = orxObject_CreateFromConfig(particleNameFromConfig);
	orxObject_SetPosition(splash, &objectVector);
}

void ProcessBallAndDirectionRolloverCollision(orxOBJECT *directionRollOverObject, orxSTRING collidedBodyPart){
	directionrolloverelement *dr = (directionrolloverelement *)orxObject_GetUserData(directionRollOverObject);
	AddToScoreAndUpdate(dr->RegisterHit(collidedBodyPart));
}

void ProcessBallAndRolloverLightCollision(orxOBJECT *rolloverLightObject){
	rolloverlightelement *ro = (rolloverlightelement *)orxObject_GetUserData(rolloverLightObject);
	AddToScoreAndUpdate(ro->RegisterHit());
	
	PlaySoundOn(rolloverLightObject, (orxCHAR*)"ElementEffect");
}

void ProcessBallAndTargetLeftCollision(orxOBJECT *ballObject, orxOBJECT *targetLeftObject){
	targetleftelement *te = (targetleftelement *)orxObject_GetUserData(targetLeftObject);
	AddToScoreAndUpdate(te->RegisterHit());
	
	PlaySoundOn(targetLeftObject, (orxCHAR*)"ElementEffect");
}

void ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject){
	targetrightelement *te = (targetrightelement *)orxObject_GetUserData(targetRightObject);
	AddToScoreAndUpdate(te->RegisterHit());
	
	PlaySoundOn(targetRightObject, (orxCHAR*)"ElementEffect");
}

void ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject){
	slingshotelement *sr = (slingshotelement *)orxObject_GetUserData(rightSlingShotObject);
	AddToScoreAndUpdate(sr->RegisterHit());
	
	orxVECTOR slingVector;
	slingVector.fX = -1600;
	slingVector.fY = -200;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	//orxLOG("vector speed %f, %f", slingVector.fX, slingVector.fY);
	
	orxObject_SetSpeed(ballObject, &slingVector);
	
	PlaySoundOn(rightSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject){
	slingshotelement *sl = (slingshotelement *)orxObject_GetUserData(leftSlingShotObject);
	AddToScoreAndUpdate(sl->RegisterHit());
	
	orxVECTOR slingVector;
	slingVector.fX = 1600;
	slingVector.fY = -200;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	
	orxObject_SetSpeed(ballObject, &slingVector);
	
	PlaySoundOn(leftSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void ProcessBallAndBumperCollision(orxOBJECT *ballObject, orxOBJECT *bumperObject, orxVECTOR hitVector){
	bumperelement *be = (bumperelement *)orxObject_GetUserData(bumperObject);
	AddToScoreAndUpdate(be->RegisterHit());
	
	PlaySoundOn(bumperObject, (orxCHAR*)"BumperEffect"); //tableObject
	
	hitVector.fX = -hitVector.fX * 1200;
	hitVector.fY = -hitVector.fY * 1200;
	
	orxObject_SetSpeed(ballObject, &hitVector);
}

/* Only creates a ball if none is in the channel. */
void CreateBall(){
	orxLOG("CreateBall +");
	orxVECTOR ballPickVector;
	ballPickVector.fX = 485;
	ballPickVector.fY = 645;
	ballPickVector.fZ = 0;
	
	orxOBJECT *objectUnderPick = GetABallObjectIntheChannel();
	
	if (objectUnderPick == orxNULL){
		//const orxSTRING objectStringName;
		//objectStringName = orxObject_GetName(objectUnderPick);
		
		//if (orxString_Compare(objectStringName, "BallObject") != 0 && IsGameOver() == orxFALSE){
		if (IsGameOver() == orxFALSE){
			ballObject = orxObject_CreateFromConfig("BallObject");
			PlaySoundOn(ballObject, (orxCHAR*)"BallLaunchEffect"); 
			SetRightTrap(orxTRUE);
			SetLauncherTrap(orxFALSE);
			if (launchMultiBalls == orxFALSE){
				DecreaseBallCount();
			} else {
				if (multiBalls <= 3){
					multiBalls++;
				}
			}
			ballInPlay = orxTRUE;
		}
	}
	
	orxLOG("CreateBall -");
	
}

/*
 * Multballs must be used up before normal balls.
 */
void DecreaseBallCount(){
	orxLOG("PrintBallCount +");
	if (balls > 0){
		balls--;
		PrintBallCount();
	}
	orxLOG("PrintBallCount -");
}

void IncreaseBallCount(int bonusScoreMarker){
	for (int x=0; x<ballIncreases.size(); x++){
		int ballIncrease = ballIncreases[x];
		if (bonusScoreMarker == ballIncrease){
			x = ballIncreases.size();
			return;
		}
	}
	ballIncreases.push_back(bonusScoreMarker);
	balls++;
	PrintBallCount();
	CreateSpanglesAtObject(ballCountObject, (orxCHAR*)"ExtraBallMainParticleObject");	
	PlaySoundOn(ballCountObject, (orxCHAR*)"BonusEffect"); 
}

void PrintBallCount(){
	std::stringstream ballCountStream;
	ballCountStream << balls;
	std::string ballCountString = ballCountStream.str();
	orxSTRING ballCountOrxString = (orxCHAR*)ballCountString.c_str();
	orxObject_SetTextString(ballCountObject, ballCountOrxString);
}

void FlashLeftFlukeLight(){
	orxObject_SetCurrentAnim(flukeLeftObject, "FlukeLeftFlashAnim");
}

void FlashRightFlukeLight(){
	orxObject_SetCurrentAnim(flukeRightObject, "FlukeRightFlashAnim");
}

orxOBJECT* GetABallObjectAtThePlunger(){
	
	orxVECTOR ballPickVector;
	ballPickVector.fX = 878;
	ballPickVector.fY = 1185;
	ballPickVector.fZ = -1.0;
	
	orxOBOX ballBoxArea;
	
	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;
	
	orxVECTOR position;
	position.fX = 854;//854;
	position.fY = 1150;//1100;
	position.fZ = -0.1;	
	
	orxVECTOR size;
	size.fX = 21;
	size.fY = 160;
	size.fZ = 1;	
	
	orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);
	
	orxU32 ballGroupID = orxCamera_GetGroupID(pstCamera, 1);
	
	orxOBJECT *ballToShoot = orxObject_BoxPick(&ballBoxArea, ballGroupID);
	return ballToShoot;
	
}

orxOBJECT* GetABallObjectIntheChannel(){
	
	orxOBOX ballBoxArea;
	
	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;
	
	orxVECTOR position;
	position.fX = 854;
	position.fY = 356;
	position.fZ = -0.1;	
	
	orxVECTOR size;
	size.fX = 42;
	size.fY = 850;
	size.fZ = 1;	
	
	orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);
	
	orxU32 ballGroupID = orxCamera_GetGroupID(pstCamera, 1);
	
	orxOBJECT *ballToShoot = orxObject_BoxPick(&ballBoxArea, ballGroupID);
	return ballToShoot;
}

void LaunchBall(){
	int strength = plung->GetPlungerStrength();
	//orxLOG("strength when launching %d", s);
	if (strength == 0)
		return;
	
	orxObject_Enable(rightTouchObject, orxFALSE);
	
	orxVECTOR ballShootVector;
	ballShootVector.fX = 0;
	ballShootVector.fY = -300 - (strength * 30);
	ballShootVector = VaryVectorY(ballShootVector, PLUNGER_SMACK_VARIANCE);
	//orxLOG("=========== launch ball %d and shootvector %f", plung->GetPlungerStrength(), ballShootVector.fY);
	
	
	orxOBJECT *ballToShoot = GetABallObjectAtThePlunger();
	//orxOBJECT *ballToShoot = orxObject_Pick(&ballPickVector, orxU32_UNDEFINED);
	
	if (ballToShoot != orxNULL){
		const orxSTRING objectName;
		objectName = orxObject_GetName(ballToShoot);
		
		if (orxString_Compare(objectName, "BallObject") == 0){
			
			orxVECTOR ballShiftUpVector; //so the ball and plunger con't collide on launch
			orxObject_GetPosition(ballToShoot, &ballShiftUpVector);
			ballShiftUpVector.fY = ballShiftUpVector.fY - 100;//50;
			orxObject_SetPosition(ballToShoot, &ballShiftUpVector);
			//orxObject_SetSpeed(ballToShoot, &ballShootVector);
					
			orxObject_SetSpeed(ballToShoot, &ballShootVector);
					
			//plung->SetToSmackUpPosition();
			
			PlaySoundOn(ballToShoot, (orxCHAR*)"BallLaunchEffect"); //tableObject
		} else {
			orxLOG("COULDNT LOCATE BALL");
		}
	} else {
		orxLOG("COULDNT LOCATE ANY OBJECT");
	}
	
	plung->RestorePosition();
	orxObject_Enable(rightTouchObject, orxTRUE);
	
}

void DestroyBall(orxOBJECT *ballObject){
	orxObject_SetLifeTime(ballObject, 0); 
	
	if (multiBalls == 0){
		ballInPlay = orxFALSE;
	}
	
	PlaySoundOn(tableObject, (orxCHAR*)"BallLostEffect");
	
	TurnOffAllMultiplierLights();
}


void SetLauncherTrap(orxBOOL close){
	launcherTrapOn = close;
	if (launcherTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapObject, 10.4);
	} else {
		orxObject_SetAngularVelocity(trapObject, -10.4);
	}
}
void SetLeftTrap(orxBOOL close){
	leftTrapOn = close;
	if (leftTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapLeftObject, -20.8);
	}
}
void SetRightTrap(orxBOOL close){
	rightTrapOn = close;
	if (rightTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapRightObject, 20.8);
	}
}

void AddToScore(int points){
	score += (points * GetMultiplier());
}

void AddToScoreAndUpdate(int points){
	AddToScore(points);
	PrintScore();
	ProcessEventsBasedOnScore();
}

void ProcessEventsBasedOnScore(){
	
	if (score > 10000000){
		IncreaseBallCount(10000000);
	}
	if (score > 5000000){
		IncreaseBallCount(5000000);
	}
	if (score > 3000000){
		IncreaseBallCount(3000000);
	}
	if (score > 2000000){
		IncreaseBallCount(2000000);
	}
	if (score > 1000000){
		IncreaseBallCount(1000000);
	}	
	if (score > 500000){
		IncreaseBallCount(500000);
	}	
	
	int multiplier = GetMultiplier();
	if (multiplier == 6){
		return; //don't bother if on 6. Nothing more player can achieve.
	}
	
}

void TurnOffAllMultiplierLights(){
	//orxLOG("TurnOffAllMultiplierLights +");
	orxObject_Enable(x2Object, orxFALSE);
	orxObject_Enable(x3Object, orxFALSE);
	orxObject_Enable(x4Object, orxFALSE);
	orxObject_Enable(x5Object, orxFALSE);
	orxObject_Enable(x6Object, orxFALSE);
	//orxLOG("TurnOffAllMultiplierLights -");
}

void IncreaseMultiplier(){
	int multiplier = GetMultiplier();
	if (multiplier < 6)
		multiplier++;
	
	TurnOffAllMultiplierLights();
	
	switch (multiplier) {
		case 6:
			orxObject_Enable(x6Object, orxTRUE);
		case 5:
			orxObject_Enable(x5Object, orxTRUE);
		case 4:
			orxObject_Enable(x4Object, orxTRUE);
		case 3:
			orxObject_Enable(x3Object, orxTRUE);
		case 2:
			orxObject_Enable(x2Object, orxTRUE);
	}
	
}

int GetMultiplier(){
	
	if (orxObject_IsEnabled(x6Object)){
		return 6;
	}
	if (orxObject_IsEnabled(x5Object)){
		return 5;
	}
	if (orxObject_IsEnabled(x4Object)){
		return 4;
	}
	if (orxObject_IsEnabled(x3Object)){
		return 3;
	}
	if (orxObject_IsEnabled(x2Object)){
		return 2;
	}
	
	return 1;
}

//1,123,456
void PrintScore(){
	//orxLOG("PrintScore +");
	std::stringstream scoreStream;
	std::stringstream formattedScoreStream;
	scoreStream << score;
	std::string str = scoreStream.str();
	
	int thousandCounter = 3;
	for (int x=str.length()-1; x>=0; x--){
		orxCHAR cc = (orxCHAR)str[x];
		formattedScoreStream << cc;
		if (thousandCounter == 0){
			thousandCounter = 3;
			str.insert(x+1, ",");
		}
		thousandCounter--;
	}
	
	int length = str.length();
	int paddingLength = 11 - length;
	std::string paddingString = scorePadding.substr(0, paddingLength);

	str = paddingString + str;
	orxSTRING s = (orxCHAR*)str.c_str();
	
	orxObject_SetTextString(scoreObject, s);
	//orxLOG("PrintScore -");
	
}



orxVECTOR FlipVector(orxVECTOR vector){
	vector.fX = -vector.fX;
	vector.fY = -vector.fY;
	return vector;
}

orxVECTOR VaryVector(orxVECTOR vector, int maxVariance){
	orxFLOAT variance = orxMath_GetRandomFloat(-maxVariance/2, maxVariance/2);
	vector.fX = vector.fX + variance;
	vector.fY = vector.fY + variance;
	
	return vector;
}

orxVECTOR VaryVectorY(orxVECTOR vector, int maxVariance){
	orxFLOAT variance = orxMath_GetRandomFloat(-maxVariance/2, maxVariance/2);
	vector.fY = vector.fY + variance;
	
	return vector;
}



/** Update callback for handling states after keypresses
 */
void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext){
	//orxLOG("===== Update +"); 
	
	if (leftFlipperPressed == orxTRUE){
		//orxLOG("leftFlipperPressed == orxTRUE");
		orxFLOAT rotation = orxObject_GetRotation(leftFlipperObject);
		if (rotation < -0.9){
			orxObject_SetAngularVelocity(leftFlipperObject, 0.0);
			orxObject_SetRotation(leftFlipperObject, -0.9);
		}
	}

	if (leftFlipperPressed == orxFALSE){
		//orxLOG("leftFlipperPressed == orxFALSE");
		orxFLOAT rotation = orxObject_GetRotation(leftFlipperObject);
		orxObject_SetAngularVelocity(leftFlipperObject, 0.0);
		
		if (rotation < 0){
			rotation += 0.2;
			orxObject_SetRotation(leftFlipperObject, rotation);
		} else if(rotation > 0){
			orxObject_SetRotation(leftFlipperObject, 0.0);
		}
		
	}

	if (rightFlipperPressed == orxTRUE){
		orxFLOAT rotation = orxObject_GetRotation(rightFlipperObject);
		if (rotation >= 0.9){
			orxObject_SetAngularVelocity(rightFlipperObject, 0.0);
			orxObject_SetRotation(rightFlipperObject, 0.9);
		}
	}

	if (rightFlipperPressed == orxFALSE){
		orxFLOAT rotation = orxObject_GetRotation(rightFlipperObject);
		orxObject_SetAngularVelocity(rightFlipperObject, 0.0);
		
		if (rotation >= 0.2){
			rotation -= 0.2;
			orxObject_SetRotation(rightFlipperObject, rotation);
		} else if(rotation >= 0){
			orxObject_SetRotation(rightFlipperObject, 0.0);
		}
	}
	
	if (launcherTrapOn == orxTRUE){
		
		orxFLOAT rotation = orxObject_GetRotation(trapObject);
		if (rotation >= 0.9){
			orxObject_SetAngularVelocity(trapObject, 0.0);
			orxObject_SetRotation(trapObject, 0.9);
		}
	}
	if (launcherTrapOn == orxFALSE){
		orxFLOAT rotation = orxObject_GetRotation(trapObject);
		if (rotation <= 0){
			orxObject_SetAngularVelocity(trapObject, 0.0);
			orxObject_SetRotation(trapObject, 0.0);
		}
	}
	
	if (leftTrapOn == orxTRUE){
		orxFLOAT rotation = orxObject_GetRotation(trapLeftObject);
		if (rotation < -0.9){
			orxObject_SetAngularVelocity(trapLeftObject, 0.0);
			orxObject_SetRotation(trapLeftObject, -0.9);
		}
	}
	if (leftTrapOn == orxFALSE){
		orxFLOAT rotation = orxObject_GetRotation(trapLeftObject);
		orxObject_SetAngularVelocity(trapLeftObject, 0.0);
		
		if (rotation < 0){
			rotation += 0.2;
			orxObject_SetRotation(trapLeftObject, rotation);
		} else {
			orxObject_SetRotation(trapLeftObject, 0.0);
		}
	}
	
	if (rightTrapOn == orxTRUE){
		orxFLOAT rotation = orxObject_GetRotation(trapRightObject);
	
		if (rotation >= 0.9){
			orxObject_SetAngularVelocity(trapRightObject, 0.0);
			orxObject_SetRotation(trapRightObject, 0.9);
		}
	}
	if (rightTrapOn == orxFALSE){
		orxFLOAT rotation = orxObject_GetRotation(trapRightObject);
		orxObject_SetAngularVelocity(trapRightObject, 0.0);
		
		if (rotation >= 0.2){
			rotation -= 0.2;
			orxObject_SetRotation(trapRightObject, rotation);
		} else if(rotation >= 0){
			orxObject_SetRotation(trapRightObject, 0.0);
		}
	}
	
	if (plung->plungerIsBeingPulled == orxTRUE){
		plung->PullDown();
	}
	if (plung->plungerIsBeingPulled == orxFALSE){
	}	

	//orxLOG("===== Update -"); 


}


void SetupTouchZonesValues(orxFLOAT screenWidth, orxFLOAT screenHeight){

	TOUCH_WIDTH = screenWidth / 2;
	TOUCH_HEIGHT = screenHeight / 2;
	TOUCH_LEFT_X = 0;
	TOUCH_Y = screenHeight / 2;
	TOUCH_RIGHT_X = screenWidth / 2;
	
}


void SetTouchHighlightPositions(orxFLOAT screenWidth, orxFLOAT screenHeight){

	orxLOG("touch x: %f by %f", screenWidth, screenHeight);
	
	
	
	orxVECTOR leftTouchPosition;
	leftTouchPosition.fX = 0;
	leftTouchPosition.fY = 1280;
	leftTouchPosition.fZ = -0.2;
	
	orxVECTOR rightTouchPosition;
	rightTouchPosition.fX = 960;
	rightTouchPosition.fY = 1280;
	rightTouchPosition.fZ = -0.2;
	
	orxLOG("y pos x: %f", leftTouchPosition.fY);
	
	orxObject_SetPosition(leftTouchObject, &leftTouchPosition);
	orxObject_SetPosition(rightTouchObject, &rightTouchPosition);
		
}


orxFLOAT GetSuitableFrustumWidthFromCurrentDevice(){
	orxFLOAT deviceWidth = 865; //default
	orxFLOAT deviceHeight = 1280; //default
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;
	
	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE){
		
		//get aspect of the current device resolution or window size:
		orxFLOAT deviceAspect = deviceWidth / deviceHeight;
		
		suitableFrustumWidth = 1280 * deviceAspect;
	}
	
	return suitableFrustumWidth;
}

orxFLOAT GetSuitableFrustumHeightFromCurrentDevice(){
	orxFLOAT deviceWidth = 865;
	orxFLOAT deviceHeight = 1280;
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;
	
	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE){
		
		//get aspect of the current device resolution or window size:
		orxFLOAT deviceAspect = deviceWidth / deviceHeight;
		
		suitableFrustumHeight = 865 / deviceAspect;
	}
	
	return suitableFrustumHeight;
}

/* This changes the frustum slightly to show a little decorative edge if the device shows black bars on aspect. */
void SetFrustumToWhateverDisplaySizeCurrentlyIs(){
	
	/**
	 * At the end, the frustrum is the thing that changes
	 * 
	 * screen is 600 x 800, aspect is: 0.75
	 * frust is 865 x 1280
	 * aspectFrustWidth = 1280 * 0.75 =  960, so potential frustrum is 960 x 1280
	 * //but 960 > 865.
	 * //So 854 will be the frust width
	 * //frust height will be 865 / 0.75 = 1138
	 * 
	 * 
	 * screen is 380 x 800, aspect is: 0.475
	 * frust is 865 x 1280
	 * aspectFrustWidth = 1280 * 0.75 =  608, so potential frustrum is 608 x 1280
	 * 608 much less than 865 so stick with 865
	 * frustrum height stays at 1280
	 * 
	 */
	
	orxFLOAT deviceWidth = 865; //default
	orxFLOAT deviceHeight = 1280; //default
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;
	
	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE){
		suitableFrustumWidth = GetSuitableFrustumWidthFromCurrentDevice();
		suitableFrustumHeight = 1280;
		
		if (suitableFrustumWidth < 865){
				
			suitableFrustumWidth = 865;
			//suitableFrustumHeight = GetSuitableFrustumHeightFromCurrentDevice();
				
		}
	
		orxLOG("SetFrustumToWhateverDisplaySizeCurrentlyIs size: %f x %f", suitableFrustumWidth, suitableFrustumHeight);
		
		orxCamera_SetFrustum(pstCamera, suitableFrustumWidth, suitableFrustumHeight, 0, 2);
	}
}

void SetProgressPercent(orxFLOAT percent){
	orxFLOAT max = 637;
	
	if (loadingProgressObject != orxNULL){
		orxFLOAT pixels = max * percent / 100;
		if (pixels == 0)
			pixels = 1;
		orxVECTOR scale;
		scale.fX = pixels;
		scale.fY = 1;
		scale.fZ = 0;
		orxObject_SetScale(loadingProgressObject, &scale);
	}
}


static orxBOOL orxFASTCALL SaveHighScoreToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption) 
{ 
  // Return orxTRUE for the section "Save", orxFALSE otherwise 
  // -> filters out everything but the "Save" section 
  return (orxString_Compare(_zSectionName, "Highscore") == 0) ? orxTRUE : orxFALSE; 
}


int GetSavedHighScore(){
	/* Does the highscore section exist? It may not have been created or loaded. Try loading first. */ 
	orxBOOL hasIt = orxConfig_HasSection("Highscore");
	
	if (!orxConfig_HasSection("Highscore")){
 		orxConfig_Load("save.ini");
	}
	
	if(orxConfig_PushSection("Highscore")){
		const orxU64 highScore = orxConfig_GetU64("Highscore");
		orxConfig_PopSection();
		
		return highScore;
	}
	
	return 0;
}


void SaveHighScoreToConfig(){
	if(orxConfig_PushSection("Highscore")){
		const orxSTRING platform = orxConfig_GetString("Highscore");
		
		std::stringstream scoreStream;
		scoreStream << score;
		std::string scoreString = scoreStream.str();
		orxSTRING scoreOrxString = (orxCHAR*)scoreString.c_str();
		
		orxConfig_SetString("Highscore", scoreOrxString); 
		orxConfig_PopSection();
		
		orxConfig_Save("save.ini", orxTRUE, SaveHighScoreToConfigFileCallback); 		
	}	
}

void TryUpdateHighscore(){
	if (score > GetSavedHighScore()){
		SaveHighScoreToConfig();
	}
}

void DeterminePlatform(){
	
	if(orxConfig_PushSection("Platform")){
		const orxSTRING platform = orxConfig_GetString("Platform");
		if (orxString_Compare(platform, "Android") == 0){
			isAndroid = orxTRUE;
		}
		
		orxConfig_PopSection();
	}
	
}


/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
  //orxLOG("run() +");
  orxSTATUS eResult = orxSTATUS_SUCCESS;
  if(orxInput_IsActive("Quit")){
    eResult = orxSTATUS_FAILURE;
  }

  //orxLOG("run() -");

  return eResult;
}

void orxFASTCALL Exit()
{ 
	orxLOG("=================== EXIT ================");
}

/** Inits the tutorial
 */
orxSTATUS orxFASTCALL Init()
{
	
	isAndroid = orxFALSE;
	DeterminePlatform();
	
	loadingPageObject = orxObject_CreateFromConfig("LoadingPageObject"); //loading-screen-dot.png
	loadingProgressObject = orxObject_CreateFromConfig("LoadingPageProgressObject"); //progress-bar.png
	endTextureLoad = orxFALSE; //reset for android restarts
	texturesToLoadCount = 0; //reset for android restarts
	
	orxEvent_AddHandler(orxEVENT_TYPE_TEXTURE, TextureLoadingEventHandler);
	orxFLOAT deviceWidth = 865;
	orxFLOAT deviceHeight = 1280;
	
	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	SetupTouchZonesValues(deviceWidth, deviceHeight);
	
	
	#ifndef __orxDEBUG__ 
		orxDEBUG_ENABLE_LEVEL(orxDEBUG_LEVEL_LOG, orxFALSE); 
	#endif // __orxDEBUG__
	
	orxLOG("=================== INIT ================");
	
	orxCLOCK       *pstClock;
	orxCLOCK       *pstSecondsClock;
	orxU32          i;
	orxINPUT_TYPE   eType;
	orxENUM         eID;

	leftFlipperPressed = orxFALSE;
	rightFlipperPressed = orxFALSE;
	launcherTrapOn = orxFALSE;
	leftTrapOn = orxFALSE;
	rightTrapOn = orxFALSE;
	ballInPlay = orxFALSE;
	
	/* Multiball variables that need to be filled to award multiball. */
	launchMultiBalls = orxFALSE;
	rollOversFilled = orxFALSE;
	targetGroupLeftFilled = orxFALSE;
	targetGroupRightFilled = orxFALSE;
	
	score = 0;
	balls = 0;

	/* Creates viewport */
	pstViewport = orxViewport_CreateFromConfig("Viewport");


	/* Gets camera */
	pstCamera = orxViewport_GetCamera(pstViewport);
	//orxCamera_SetZoom(pstCamera, nativeScreenHeight/1280);

	SetFrustumToWhateverDisplaySizeCurrentlyIs();

	/* Registers event handler */
	orxEvent_AddHandler(orxEVENT_TYPE_ANIM, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_INPUT, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_SYSTEM, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_VIEWPORT, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_USER_DEFINED, CustomEventHandler);

	tableObject = orxObject_CreateFromConfig("TableObject");
	
	SetProgressPercent(0);
	
	leftFlipperObject = orxObject_CreateFromConfig("LeftFlipperObject");
	rightFlipperObject = orxObject_CreateFromConfig("RightFlipperObject");
	ballCatcherObject = orxObject_CreateFromConfig("BallCatcherObject");
	
	trapObject = orxObject_CreateFromConfig("TrapObject");
	SetLauncherTrap(orxFALSE);
	
	trapLeftObject = orxObject_CreateFromConfig("TrapLeftObject");
	trapRightObject = orxObject_CreateFromConfig("TrapRightObject");
	
	flukeLeftObject = orxObject_CreateFromConfig("FlukeLeftObject");
	flukeRightObject = orxObject_CreateFromConfig("FlukeRightObject");
	
	beanPoleObject = orxObject_CreateFromConfig("BeanPoleObject");
	x6Object = orxOBJECT(orxObject_GetChild(beanPoleObject));
	x5Object = orxOBJECT(orxObject_GetSibling(x6Object));
	x4Object = orxOBJECT(orxObject_GetSibling(x5Object));
	x3Object = orxOBJECT(orxObject_GetSibling(x4Object));
	x2Object = orxOBJECT(orxObject_GetSibling(x3Object));
	TurnOffAllMultiplierLights();
	
	directionrolloverelement *launcherRollOver = new directionrolloverelement(50, (orxCHAR*)"BALL_LAUNCHED", orxNULL);
	directionrolloverelement *leftChannelRollOver = new directionrolloverelement(50, orxTRUE, (orxCHAR*)"LEFT_FLUKE_SHOT", orxNULL);
	leftChannelRollOver->SetActivatorPosition(142, 982);
	directionrolloverelement *rightChannelRollOver = new directionrolloverelement(50, orxTRUE, (orxCHAR*)"RIGHT_FLUKE_SHOT", orxNULL);
	rightChannelRollOver->SetActivatorPosition(797, 982);
	
	bumperelement *be = new bumperelement(1000);
	be->SetActivatorPosition(346, 526);
	
	bumperelement *be2 = new bumperelement(1000);
	be2->SetActivatorPosition(604, 526);
	
	bumperelement *be3 = new bumperelement(1000);
	be3->SetActivatorPosition(464, 708);
	
	const slingshotelement *sle = new slingshotelement(250, (orxCHAR *)"SlingShotLeftObject", (orxCHAR *)"SlingShotLeftHitAnim");
	const slingshotelement *sre = new slingshotelement(250, (orxCHAR *)"SlingShotRightObject", (orxCHAR *)"SlingShotRightHitAnim");

	int tgX = 122;
	int tgY = 416;
	targetGroupLeft = new elementgroup();
	targetGroupLeft->SetEventName((orxCHAR*)"TARGET_LEFT_GROUP_FILLED");

	targetleftelement *te1 = new targetleftelement(500);
	te1->SetActivatorPosition(tgX-8, tgY+9);
	te1->SetLightPosition(tgX+10, tgY);
	targetleftelement *te2 = new targetleftelement(500);
	te2->SetActivatorPosition(tgX-8, tgY+49);
	te2->SetLightPosition(tgX+10, tgY+44);
	targetleftelement *te3 = new targetleftelement(500);
	te3->SetActivatorPosition(tgX-8, tgY+119);
	te3->SetLightPosition(tgX+10, tgY+110);
	targetleftelement *te4 = new targetleftelement(500);
	te4->SetActivatorPosition(tgX-8, tgY+160);
	te4->SetLightPosition(tgX+10, tgY+154);
	targetGroupLeft->Add(te1);
	targetGroupLeft->Add(te2);
	targetGroupLeft->Add(te3);
	targetGroupLeft->Add(te4);
	
	tgX = 754;
	tgY = 618;
	targetGroupRight = new elementgroup();
	targetGroupRight->SetEventName((orxCHAR*)"TARGET_RIGHT_GROUP_FILLED");

	targetrightelement *te5 = new targetrightelement(500);
	te5->SetActivatorPosition(tgX+56, tgY+6);
	te5->SetLightPosition(tgX, tgY);
	targetrightelement *te6 = new targetrightelement(500);
	te6->SetActivatorPosition(tgX+56, tgY+47);
	te6->SetLightPosition(tgX, tgY+44);
	targetGroupRight->Add(te5);
	targetGroupRight->Add(te6);


	tgX = 327;
	tgY = 250;

	rolloverlightgroup = new elementgroup();
	rolloverlightgroup->SetEventName((orxCHAR*)"ROLLOVER_GROUP_FILLED");
	
	rolloverlightelement *ro1 = new rolloverlightelement(100);
	ro1->SetActivatorPosition(tgX, tgY);
	rolloverlightelement *ro2 = new rolloverlightelement(100);
	ro2->SetActivatorPosition(tgX + 100, tgY);
	rolloverlightelement *ro3 = new rolloverlightelement(100);
	ro3->SetActivatorPosition(tgX + 200, tgY);
	rolloverlightelement *ro4 = new rolloverlightelement(100);
	ro4->SetActivatorPosition(tgX + 300, tgY);
	rolloverlightgroup->Add(ro1);
	rolloverlightgroup->Add(ro2);
	rolloverlightgroup->Add(ro3);
	rolloverlightgroup->Add(ro4);

	scoreObject = orxObject_CreateFromConfig("LedObject");
	ballCountObject = orxObject_CreateFromConfig("BallLedCounterObject");

	leftTouchObject = orxObject_CreateFromConfig("TouchHighlightLeftObject");
	rightTouchObject = orxObject_CreateFromConfig("TouchHighlightRightObject");
	swipeTouchObject = orxObject_CreateFromConfig("TouchHighlightSwipeObject");
	
	SetTouchHighlightPositions(deviceWidth, deviceHeight);
	
	orxObject_Enable(leftTouchObject, orxTRUE);
	orxObject_Enable(rightTouchObject, orxTRUE);

	plung = new plunger();

	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());

	int lastScore = GetSavedHighScore();

	score = lastScore;


	ShowTitle();
	//orxObject_CreateFromConfig("TestObject");

	/* Done! */
  
  	/* clocks */
	pstClock = orxClock_Create(orx2F(0.01f), orxCLOCK_TYPE_USER);
	pstSecondsClock = orxClock_Create(orx2F(1.0f), orxCLOCK_TYPE_USER);
	pstDancingLightsClock = orxClock_Create(orx2F(0.35f), orxCLOCK_TYPE_USER);

	/* Registers callbacks */
	orxClock_Register(pstClock, Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstSecondsClock, SecondsUpdate, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstDancingLightsClock, DancingLightsUpdate, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	//orxClock_Pause(pstDancingLightsClock);
  
	return orxSTATUS_SUCCESS;
}



int main(int argc, char **argv)
{
	#ifndef __orxDEBUG__ 
		orxConfig_SetEncryptionKey("0000-1111");
	#endif // __orxDEBUG__
	
	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}

#ifdef __orxMSVC__

// Here's an example for a console-less program under windows with visual studio
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  // Inits and executes orx
  orx_WinExecute(Init, Run, Exit);

  // Done!
  return EXIT_SUCCESS;
}

#endif // __orxMSVC__

/* Registers plugin entry */
//orxPLUGIN_DECLARE_ENTRY_POINT(Init);


