#include "orx.h"
#include "element.h"
#include "targetrightelement.h"

targetrightelement::targetrightelement(int points) : element(points, (orxCHAR*)"TargetRightObject", (orxCHAR*)"TargetLightObject") {
	activatorHitAnimationName = (orxCHAR*)"TargetRightHitAnim";
	lightOnAnimationName = (orxCHAR*)"TargetLightOnAnim";
	lightOffAnimationName = (orxCHAR*)"TargetLightOffAnim";
	lightFlashAnimationName = (orxCHAR*)"TargetLightFlashAnim";
}

targetrightelement::~targetrightelement()
{
}

void targetrightelement::RunRules() {
	//orxLOG("----------------targetrightelement RunRules");
	element::RunRules();
}
