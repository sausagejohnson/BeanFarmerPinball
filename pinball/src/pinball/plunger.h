#ifndef PLUNGER_H
#define PLUNGER_H

#include "orx.h"

class plunger
{

private:
	orxOBJECT *plungerObject;
	orxOBJECT *springObject;
	int plungerStrength;
	void SetSpringFrameToDisplay(int frame);
	
public:
	plunger();
	~plunger();

	orxBOOL plungerIsBeingPulled;
	void PullDown(); //call every period to pull down some more
	void RestorePosition();
	void SmackUpPosition();
	void SetToSmackUpPosition();
	int  GetPlungerStrength();
	void SetPlungerStrength(int strength);
	void SetPlungerStrengthByPercent(float percent);
	void SetPlungerLevelByPercent(float percent);
	

};

#endif // PLUNGER_H
