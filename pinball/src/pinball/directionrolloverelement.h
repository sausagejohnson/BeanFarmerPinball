#ifndef DIRECTIONROLLOVERELEMENT_H
#define DIRECTIONROLLOVERELEMENT_H

class directionrolloverelement : public element::element
{
private:
	orxDOUBLE topBodyHitTime;
	orxDOUBLE midBodyHitTime;
	orxDOUBLE bottomBodyHitTime;

	orxSTRING upEventName;
	orxSTRING downEventName;

public:
	directionrolloverelement(int p, orxSTRING upDirectionEventName, orxSTRING downDirectionEventName); //narrow to make the bodies closer
	directionrolloverelement(int p, orxBOOL narrow, orxSTRING upDirectionEventName, orxSTRING downDirectionEventName); //narrow to make the bodies closer
	~directionrolloverelement();
	void RunRules();
	int RegisterHit(orxSTRING bodyNameHit);
};

#endif // DIRECTIONROLLOVERELEMENT_H
