#ifndef ELEMENTGROUP_H
#define ELEMENTGROUP_H

#include <vector>

class element;

class elementgroup
{
private:
	std::vector<element*> elements;
	orxSTRING eventName;

public:
	elementgroup();
	~elementgroup();

	void Add(element *e); //add element and return new count
	int RunGroupRules();
	int LightsOnCount();
	void TurnAllLightsOff();
	void FlashAllLights();
	void FlashAllActivators();
	void TurnActivatorOnAt(int index);
	void TurnLightOnAt(int index);
	int ActivatorsOnCount();
	void TurnAllActivatorsOff();
	void ShiftActivatorsRight();
	void ShiftLightsRight();
	void DoSpanglesOnActivators();
	void DoSpanglesOnLights();
	void SetEventName(orxSTRING eventName);
};

#endif // ELEMENTGROUP_H
