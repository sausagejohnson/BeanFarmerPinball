#include "orx.h"
#include "element.h"

element::element(int p, orxSTRING activatorNameFromConfig, orxSTRING lightNameFromConfig) {
	activator = orxObject_CreateFromConfig(activatorNameFromConfig);
	/*if (activator == orxNULL){
		orxLOG("=== The created activator %s is still null for some reason.", activatorNameFromConfig);
	} else {
		orxLOG("=== Activator %s created properly.", activatorNameFromConfig);
	}*/
		
	light = orxNULL;
	if (lightNameFromConfig != orxNULL){
		light = orxObject_CreateFromConfig(lightNameFromConfig);
		/*if (light == orxNULL){
			orxLOG("The created light %s is still null for some reason.", lightNameFromConfig);
		} else {
			orxLOG("=== Light %s created properly.", lightNameFromConfig);
		}*/
	}
	
	/* Safety defaults. */
	initialActivatorZ = 0.0;
	initialLightZ = 0.0;
	
	SaveZPositions();
	
	parentElementGroup = orxNULL;
	
	orxObject_SetUserData(activator, this);
	points = p;
	pointsCollected = 0;
	activatorAnimationName = orxNULL;
	activatorFlashAnimationName = orxNULL;
	lightFlashAnimationName = orxNULL;
}

element::~element() {

}

void element::SaveZPositions(){
	if (ActivatorExists()){
		orxVECTOR position;
		orxObject_GetPosition(activator, &position);
		//orxLOG("The current activator position is x:%f y:%f z:%f", position.fX, position.fY, position.fZ);
		initialActivatorZ = position.fZ;
	}
	if (LightExists()){
		orxVECTOR position;
		orxObject_GetPosition(light, &position);
		//orxLOG("The current light position is x:%f y:%f z:%f", position.fX, position.fY, position.fZ);
		initialLightZ = position.fZ;
	}
}


int element::RegisterHit() {
	this->ProcessHit();
	this->RunRules();
	
	return CollectAndClearPoints();
}

void element::RunRules() {
	pointsCollected += points;
	
	if (parentElementGroup != orxNULL){
		int groupAchievementPoints = parentElementGroup->RunGroupRules();
		pointsCollected += groupAchievementPoints;
	}

}

void element::ProcessHit(){
	TurnOnActivator();
	TurnOnLight();
	//orxLOG("anims for both %s and %s", activatorHitAnimationName, lightOnAnimationName);
}

void element::SetActivatorPosition(int x, int y){
	orxVECTOR positionVector;
	positionVector.fX = x;
	positionVector.fY = y;
	positionVector.fZ = initialActivatorZ;
	
	//orxLOG("The position is being set to x:%f y:%f z:%f", positionVector.fX, positionVector.fY, positionVector.fZ);
	
	orxObject_SetPosition(activator, &positionVector);
}

void element::SetLightPosition(int x, int y){
	orxVECTOR positionVector;
	positionVector.fX = x;
	positionVector.fY = y;
	positionVector.fZ = initialLightZ;
	
	orxObject_SetPosition(light, &positionVector);
}

/*orxVECTOR element::GetActivatorPosition(){
	orxVECTOR pos;
	orxObject_GetPosition(activator, &pos);
	
	return pos;
}

orxVECTOR element::GetLightPosition(){
	orxVECTOR pos;
	orxObject_GetPosition(light, &pos);
	
	return pos;
}*/

void element::SetElementParentGroup(elementgroup *group){
	parentElementGroup = group;
}

orxBOOL element::IsLit(){
	if (LightExists()){
		orxBOOL isLit = orxObject_IsCurrentAnim(light, lightOnAnimationName);
		return isLit;
	}
	return orxFALSE;
}

orxBOOL element::IsActivatorOn(){
	if (ActivatorExists()){
 		orxBOOL isOn = orxObject_IsCurrentAnim(activator, activatorHitAnimationName);
		return isOn;
	}
	return orxFALSE;
}

void element::TurnOffActivator(){
	if (ActivatorExists() && activatorAnimationName != orxNULL){
		orxObject_SetCurrentAnim(activator, activatorAnimationName);
	}
}

void element::TurnOnActivator(){
	if (ActivatorExists() && activatorHitAnimationName != orxNULL){
		orxObject_SetCurrentAnim(activator, activatorHitAnimationName);
	}
}

orxBOOL element::ActivatorExists(){
	return (activator != orxNULL && activatorHitAnimationName != orxNULL);
}
orxBOOL element::LightExists(){
	return (light != orxNULL && lightOnAnimationName != orxNULL);
}

void element::TurnOffLight(){
	if (LightExists()) {
		orxObject_SetTargetAnim(light, lightOffAnimationName);
	}
}

void element::TurnOnLight(){
	if (LightExists()) {
		orxObject_SetCurrentAnim(light, lightOnAnimationName);
	}
}

int element::CollectAndClearPoints(){
	int pointsToReturn = pointsCollected;
	pointsCollected = 0;
	
	return pointsToReturn;
}

void element::FlashLight(){
	if (LightExists()) {
		if (lightFlashAnimationName != orxNULL){
			orxObject_SetCurrentAnim(light, lightFlashAnimationName);
		}
	}
}

void element::FlashActivator(){
	if (ActivatorExists()) {
		if (activatorFlashAnimationName != orxNULL){
			orxObject_SetCurrentAnim(activator, activatorFlashAnimationName);
		}
	}
}

void element::CreateSpanglesOnActivator(orxSTRING particleNameFromConfig){
	CreateSpanglesAtObject(activator, particleNameFromConfig);
}

void element::CreateSpanglesOnLight(orxSTRING particleNameFromConfig){
	CreateSpanglesAtObject(light, particleNameFromConfig);
}

void element::CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig){
	orxVECTOR objectVector;
	orxObject_GetPosition(object, &objectVector);
	objectVector.fZ = -0.1;
	orxOBJECT *splash = orxObject_CreateFromConfig(particleNameFromConfig);
	orxObject_SetPosition(splash, &objectVector);
}