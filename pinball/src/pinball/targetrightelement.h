#ifndef TARGETRIGHTELEMENT_H
#define TARGETRIGHTELEMENT_H

class targetrightelement : public element::element
{
public:
	targetrightelement(int points);
	~targetrightelement();

	void RunRules();
};

#endif // TARGETRIGHTELEMENT_H
