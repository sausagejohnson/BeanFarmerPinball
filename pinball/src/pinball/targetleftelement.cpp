#include "orx.h"
#include "element.h"
#include "targetleftelement.h"

targetleftelement::targetleftelement(int points) : element(points, (orxCHAR*)"TargetLeftObject", (orxCHAR*)"TargetLightObject") {
	activatorHitAnimationName = (orxCHAR*)"TargetLeftHitAnim";
	lightOnAnimationName = (orxCHAR*)"TargetLightOnAnim";
	lightOffAnimationName = (orxCHAR*)"TargetLightOffAnim";
	lightFlashAnimationName = (orxCHAR*)"TargetLightFlashAnim";
}

targetleftelement::~targetleftelement()
{
}

void targetleftelement::RunRules() {
	//orxLOG("----------------targetleftelement RunRules");
	element::RunRules();
}



