#ifndef PINBALL_H
#define PINBALL_H

void ProcessBallAndBumperCollision(orxOBJECT *ballObject, orxOBJECT *bumperObject, orxVECTOR hitVector);
void ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject);
void ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject);
void ProcessBallAndTargetLeftCollision(orxOBJECT *ballObject, orxOBJECT *targetLeftObject);
void ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject);
void ProcessBallAndRolloverLightCollision(orxOBJECT *targetRightObject);
void ProcessBallAndDirectionRolloverCollision(orxOBJECT *directionRollOverObject, orxSTRING collidedBodyPart);
void PrintScore();

void CreateBall();
void DestroyBall(orxOBJECT *ballObject);
void DestroyBallAndCreateNewBall(orxOBJECT *ballObject);
void LaunchBall(); //launch any ball found above the plunger
void PlungerRestore();
void AddToScore(int points);
void AddToScoreAndUpdate(int points);
void ProcessEventsBasedOnScore();
int GetMultiplier(); //whats the current lit multiplier on the beanpole? values: 1-6
void IncreaseMultiplier();
void TurnOffAllMultiplierLights();
void TurnOffAllTableLights();
void SetLauncherTrap(orxBOOL close); //true to close
void SetLeftTrap(orxBOOL close); //true to close
void SetRightTrap(orxBOOL close); //true to close
void FlashLeftFlukeLight();
void FlashRightFlukeLight();
void DecreaseBallCount();
void IncreaseBallCount(int scoreBonusMaker); //save the score bonus and increase ball count so it doesn't happen twice.
void PrintBallCount();
void StrikeLeftFlipper();
void StrikeRightFlipper();
void StartNewGame();
void SetupTouchZonesValues(orxFLOAT deviceWidth, orxFLOAT deviceHeight);
void SetTouchHighlightPositions(orxFLOAT deviceWidth, orxFLOAT deviceHeight);
void SetProgressPercent(orxFLOAT percent);

void CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig);
void ShowTitleSlide(int slideNumberToShow);
void ShowTitle();
void HideTitle();
void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig);

int GetSavedHighScore();
void SaveHighScoreToConfig();
void TryUpdateHighscore();

void SetFrustumToWhateverDisplaySizeCurrentlyIs();

orxBOOL IsWithin(float x, float y, float x1, float y1, float x2, float y2); //is x,y within the x1,y1 x2, y2 box
orxBOOL IsGameOver();
orxBOOL IsTitleVisible();

orxVECTOR GetAdjustedTouchCoords(orxVECTOR touchInVector);
orxVECTOR FlipVector(orxVECTOR);
orxVECTOR VaryVector(orxVECTOR vector, int maxVariance); //vary fx and fy by x units. -x/2 to +x/2
orxVECTOR VaryVectorY(orxVECTOR vector, int maxVariance); //vary fx by x units. -x/2 to +x/2

orxOBJECT* GetABallObjectAtThePlunger();
orxOBJECT* GetABallObjectIntheChannel();

#endif


