#include "plunger.h"
#include <sstream>

const int MIN_SPRINT_FRAME = 1; 
const int MAX_SPRINT_FRAME = 20; 
const int MAX_PLUNGER_STRENGTH = 68; 
const int PLUNGER_REST_Y_POS = 1190;
const int PLUNGER_PULLED_Y_POS = 1230;


plunger::plunger()
{
	plungerObject = orxObject_CreateFromConfig("PlungerObject");
	springObject = orxObject_CreateFromConfig("SpringObject");
	plungerIsBeingPulled = orxFALSE;
	plungerStrength = 0;

	/* test */
	orxVECTOR plungerPos;
	plungerPos.fX = 864;
	plungerPos.fY = PLUNGER_REST_Y_POS;
	plungerPos.fZ = 0.0;
	orxObject_SetPosition(plungerObject, &plungerPos);
	orxVECTOR springPos;
	springPos.fX = plungerPos.fX - 10;
	springPos.fY = plungerPos.fY + 28;
	springPos.fZ = -0.1;
	orxObject_SetPosition(springObject, &springPos);
	
}

void plunger::SetSpringFrameToDisplay(int frame){
	
	std::stringstream animFrame;
	animFrame << "Spring" << frame << "Anim";
	std::string frameString = animFrame.str();
	orxObject_SetCurrentAnim(springObject, frameString.c_str());
	
	//orxLOG("animframe:::: %s", frameString.c_str());
}

void plunger::PullDown(){
	orxVECTOR plungerVector;
	orxObject_GetPosition(plungerObject, &plungerVector);
	
	if (plungerStrength < MAX_PLUNGER_STRENGTH){
		plungerStrength++;
		//orxLOG("plungerStrength %d MAX_PLUNGER_STRENGTH %d", plungerStrength, MAX_PLUNGER_STRENGTH);
	}
	
	float percent = ((float)plungerStrength / (float)MAX_PLUNGER_STRENGTH) * 100;
	
	SetPlungerLevelByPercent(percent);
}

void plunger::RestorePosition(){
	orxVECTOR plungerUpVector;
	orxObject_GetPosition(plungerObject, &plungerUpVector);
	
	plungerUpVector.fY = PLUNGER_REST_Y_POS;
	orxObject_SetPosition(plungerObject, &plungerUpVector);
	
	SetSpringFrameToDisplay(1);
	SetPlungerStrength(0);
}

void plunger::SetToSmackUpPosition(){
	orxVECTOR plungerUpVector;
	orxObject_GetPosition(plungerObject, &plungerUpVector);
	
	plungerUpVector.fY = 640; //660 at peak
	orxObject_SetPosition(plungerObject, &plungerUpVector);
}

void plunger::SetPlungerLevelByPercent(float percent){
	int frame = (int)(((MAX_SPRINT_FRAME-1) * (percent/100)) + 1);
	SetSpringFrameToDisplay(frame);
	
	orxVECTOR plungerVector;
	orxObject_GetPosition(plungerObject, &plungerVector);
	
	float difference = PLUNGER_PULLED_Y_POS - PLUNGER_REST_Y_POS;
	float differenceSize = difference * (percent/100);
	
	plungerVector.fY = PLUNGER_REST_Y_POS + differenceSize;
	orxObject_SetPosition(plungerObject, &plungerVector);
}

void plunger::SetPlungerStrength(int strength){
	plungerStrength = strength;
	//orxLOG("SetPlungerStrength %d", strength);
}

void plunger::SetPlungerStrengthByPercent(float percent){
	float strength = ((percent/100) * (float)MAX_PLUNGER_STRENGTH);
	//orxLOG("Settings strength %f percent %f", strength, percent);
	SetPlungerStrength(strength);
}

int plunger::GetPlungerStrength(){
	return plungerStrength;
}


plunger::~plunger()
{
}

