##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=pinball
ConfigurationName      :=Debug
WorkspacePath          := "D:\pin\pinball\build\codelite"
ProjectPath            := "D:\pin\pinball\build\codelite\pinball"
IntermediateDirectory  :=../../../bin/mingw
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=wayne.johnson
Date                   :=8/09/2014
CodeLitePath           :="C:\Program Files (x86)\CodeLite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="D:\pin\pinball\build\codelite\pinball\pinball.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
LinkOptions            :=   -W1,--export-all-symbols
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd.dll" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib/mingw 

##
## Common variables
## AR, CXX, CC, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
UNIT_TEST_PP_SRC_DIR:=C:\UnitTest++-1.3
Objects=$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix) $(IntermediateDirectory)/pinball_element$(ObjectSuffix) $(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix) $(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_plunger$(ObjectSuffix) \
	

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects) > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "../../../bin/mingw"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix): ../../../src/pinball/pinball.cpp $(IntermediateDirectory)/pinball_pinball$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/pinball.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_pinball$(DependSuffix): ../../../src/pinball/pinball.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_pinball$(DependSuffix) -MM "D:/pin/pinball/src/pinball/pinball.cpp"

$(IntermediateDirectory)/pinball_pinball$(PreprocessSuffix): ../../../src/pinball/pinball.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_pinball$(PreprocessSuffix) "D:/pin/pinball/src/pinball/pinball.cpp"

$(IntermediateDirectory)/pinball_element$(ObjectSuffix): ../../../src/pinball/element.cpp $(IntermediateDirectory)/pinball_element$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/element.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_element$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_element$(DependSuffix): ../../../src/pinball/element.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_element$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_element$(DependSuffix) -MM "D:/pin/pinball/src/pinball/element.cpp"

$(IntermediateDirectory)/pinball_element$(PreprocessSuffix): ../../../src/pinball/element.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_element$(PreprocessSuffix) "D:/pin/pinball/src/pinball/element.cpp"

$(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix): ../../../src/pinball/elementgroup.cpp $(IntermediateDirectory)/pinball_elementgroup$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/elementgroup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_elementgroup$(DependSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_elementgroup$(DependSuffix) -MM "D:/pin/pinball/src/pinball/elementgroup.cpp"

$(IntermediateDirectory)/pinball_elementgroup$(PreprocessSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_elementgroup$(PreprocessSuffix) "D:/pin/pinball/src/pinball/elementgroup.cpp"

$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix): ../../../src/pinball/bumperelement.cpp $(IntermediateDirectory)/pinball_bumperelement$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/bumperelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_bumperelement$(DependSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_bumperelement$(DependSuffix) -MM "D:/pin/pinball/src/pinball/bumperelement.cpp"

$(IntermediateDirectory)/pinball_bumperelement$(PreprocessSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_bumperelement$(PreprocessSuffix) "D:/pin/pinball/src/pinball/bumperelement.cpp"

$(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix): ../../../src/pinball/targetleftelement.cpp $(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/targetleftelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix): ../../../src/pinball/targetleftelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix) -MM "D:/pin/pinball/src/pinball/targetleftelement.cpp"

$(IntermediateDirectory)/pinball_targetleftelement$(PreprocessSuffix): ../../../src/pinball/targetleftelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_targetleftelement$(PreprocessSuffix) "D:/pin/pinball/src/pinball/targetleftelement.cpp"

$(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix): ../../../src/pinball/targetrightelement.cpp $(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/targetrightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix) -MM "D:/pin/pinball/src/pinball/targetrightelement.cpp"

$(IntermediateDirectory)/pinball_targetrightelement$(PreprocessSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_targetrightelement$(PreprocessSuffix) "D:/pin/pinball/src/pinball/targetrightelement.cpp"

$(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix): ../../../src/pinball/rolloverlightelement.cpp $(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/rolloverlightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix) -MM "D:/pin/pinball/src/pinball/rolloverlightelement.cpp"

$(IntermediateDirectory)/pinball_rolloverlightelement$(PreprocessSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_rolloverlightelement$(PreprocessSuffix) "D:/pin/pinball/src/pinball/rolloverlightelement.cpp"

$(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix): ../../../src/pinball/directionrolloverelement.cpp $(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/directionrolloverelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix) -MM "D:/pin/pinball/src/pinball/directionrolloverelement.cpp"

$(IntermediateDirectory)/pinball_directionrolloverelement$(PreprocessSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_directionrolloverelement$(PreprocessSuffix) "D:/pin/pinball/src/pinball/directionrolloverelement.cpp"

$(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix): ../../../src/pinball/slingshotelement.cpp $(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/slingshotelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix) -MM "D:/pin/pinball/src/pinball/slingshotelement.cpp"

$(IntermediateDirectory)/pinball_slingshotelement$(PreprocessSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_slingshotelement$(PreprocessSuffix) "D:/pin/pinball/src/pinball/slingshotelement.cpp"

$(IntermediateDirectory)/pinball_plunger$(ObjectSuffix): ../../../src/pinball/plunger.cpp $(IntermediateDirectory)/pinball_plunger$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "D:/pin/pinball/src/pinball/plunger.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_plunger$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_plunger$(DependSuffix): ../../../src/pinball/plunger.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_plunger$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_plunger$(DependSuffix) -MM "D:/pin/pinball/src/pinball/plunger.cpp"

$(IntermediateDirectory)/pinball_plunger$(PreprocessSuffix): ../../../src/pinball/plunger.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_plunger$(PreprocessSuffix) "D:/pin/pinball/src/pinball/plunger.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/pinball_pinball$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_pinball$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_pinball$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_element$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_element$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_element$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_elementgroup$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_elementgroup$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_bumperelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_bumperelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetleftelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetrightelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_rolloverlightelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_directionrolloverelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_slingshotelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_plunger$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_plunger$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_plunger$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) $(OutputFile).exe
	$(RM) "D:\pin\pinball\build\codelite\.build-win32_dynamic_release\pinball"


