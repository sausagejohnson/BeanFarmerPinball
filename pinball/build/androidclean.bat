REM -- Clear expect files from folders first.

cd android\app\src\main
IF NOT EXIST .\assets GOTO NEXT
cd assets
del /Q *.png
del /Q *.ogg
del /Q *.ini
cd ..
cd ..

:NEXT
cd jni
del /Q *.cpp
del /Q *.h
cd ..

:QUIT
cd ..
cd ..
cd ..
REM pause
