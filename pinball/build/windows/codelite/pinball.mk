##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=pinball
ConfigurationName      :=Release
WorkspacePath          := "C:\Work\Downloads\Dev\pin\pinball\build\windows\codelite"
ProjectPath            := "C:\Work\Downloads\Dev\pin\pinball\build\windows\codelite"
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=WJohnson
Date                   :=12/21/14
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/MinGW-4.8.1/bin/g++.exe 
SharedObjectLinkerName :=C:/MinGW-4.8.1/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/windows/pinball.exe
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="pinball.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-4.8.1/bin/windres.exe 
LinkOptions            :=  -mwindows
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orx $(LibrarySwitch)winmm 
ArLibs                 :=  "orx" "winmm" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib/windows $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-4.8.1/bin/ar.exe rcu
CXX      := C:/MinGW-4.8.1/bin/g++.exe 
CC       := C:/MinGW-4.8.1/bin/gcc.exe 
CXXFLAGS :=  -msse2 -ffast-math -g -O2 -fno-exceptions -fno-rtti $(Preprocessors)
CFLAGS   :=  -msse2 -ffast-math -g -O2 -fno-exceptions -fno-rtti $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-4.8.1/bin/as.exe 


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
UNIT_TEST_PP_SRC_DIR:=C:\UnitTest++-1.3
Objects0=$(IntermediateDirectory)/pinball_bumperelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_element.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_elementgroup.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_pinball.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_plunger.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_slingshotelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_targetleftelement.cpp$(ObjectSuffix) $(IntermediateDirectory)/pinball_targetrightelement.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/resources.rc$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/pinball_bumperelement.cpp$(ObjectSuffix): ../../../src/pinball/bumperelement.cpp $(IntermediateDirectory)/pinball_bumperelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/bumperelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_bumperelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_bumperelement.cpp$(DependSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_bumperelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_bumperelement.cpp$(DependSuffix) -MM "../../../src/pinball/bumperelement.cpp"

$(IntermediateDirectory)/pinball_bumperelement.cpp$(PreprocessSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_bumperelement.cpp$(PreprocessSuffix) "../../../src/pinball/bumperelement.cpp"

$(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(ObjectSuffix): ../../../src/pinball/directionrolloverelement.cpp $(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/directionrolloverelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(DependSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(DependSuffix) -MM "../../../src/pinball/directionrolloverelement.cpp"

$(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(PreprocessSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_directionrolloverelement.cpp$(PreprocessSuffix) "../../../src/pinball/directionrolloverelement.cpp"

$(IntermediateDirectory)/pinball_element.cpp$(ObjectSuffix): ../../../src/pinball/element.cpp $(IntermediateDirectory)/pinball_element.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/element.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_element.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_element.cpp$(DependSuffix): ../../../src/pinball/element.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_element.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_element.cpp$(DependSuffix) -MM "../../../src/pinball/element.cpp"

$(IntermediateDirectory)/pinball_element.cpp$(PreprocessSuffix): ../../../src/pinball/element.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_element.cpp$(PreprocessSuffix) "../../../src/pinball/element.cpp"

$(IntermediateDirectory)/pinball_elementgroup.cpp$(ObjectSuffix): ../../../src/pinball/elementgroup.cpp $(IntermediateDirectory)/pinball_elementgroup.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/elementgroup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_elementgroup.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_elementgroup.cpp$(DependSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_elementgroup.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_elementgroup.cpp$(DependSuffix) -MM "../../../src/pinball/elementgroup.cpp"

$(IntermediateDirectory)/pinball_elementgroup.cpp$(PreprocessSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_elementgroup.cpp$(PreprocessSuffix) "../../../src/pinball/elementgroup.cpp"

$(IntermediateDirectory)/pinball_pinball.cpp$(ObjectSuffix): ../../../src/pinball/pinball.cpp $(IntermediateDirectory)/pinball_pinball.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/pinball.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_pinball.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_pinball.cpp$(DependSuffix): ../../../src/pinball/pinball.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_pinball.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_pinball.cpp$(DependSuffix) -MM "../../../src/pinball/pinball.cpp"

$(IntermediateDirectory)/pinball_pinball.cpp$(PreprocessSuffix): ../../../src/pinball/pinball.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_pinball.cpp$(PreprocessSuffix) "../../../src/pinball/pinball.cpp"

$(IntermediateDirectory)/pinball_plunger.cpp$(ObjectSuffix): ../../../src/pinball/plunger.cpp $(IntermediateDirectory)/pinball_plunger.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/plunger.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_plunger.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_plunger.cpp$(DependSuffix): ../../../src/pinball/plunger.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_plunger.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_plunger.cpp$(DependSuffix) -MM "../../../src/pinball/plunger.cpp"

$(IntermediateDirectory)/pinball_plunger.cpp$(PreprocessSuffix): ../../../src/pinball/plunger.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_plunger.cpp$(PreprocessSuffix) "../../../src/pinball/plunger.cpp"

$(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(ObjectSuffix): ../../../src/pinball/rolloverlightelement.cpp $(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/rolloverlightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(DependSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(DependSuffix) -MM "../../../src/pinball/rolloverlightelement.cpp"

$(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(PreprocessSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_rolloverlightelement.cpp$(PreprocessSuffix) "../../../src/pinball/rolloverlightelement.cpp"

$(IntermediateDirectory)/pinball_slingshotelement.cpp$(ObjectSuffix): ../../../src/pinball/slingshotelement.cpp $(IntermediateDirectory)/pinball_slingshotelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/slingshotelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_slingshotelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_slingshotelement.cpp$(DependSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_slingshotelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_slingshotelement.cpp$(DependSuffix) -MM "../../../src/pinball/slingshotelement.cpp"

$(IntermediateDirectory)/pinball_slingshotelement.cpp$(PreprocessSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_slingshotelement.cpp$(PreprocessSuffix) "../../../src/pinball/slingshotelement.cpp"

$(IntermediateDirectory)/pinball_targetleftelement.cpp$(ObjectSuffix): ../../../src/pinball/targetleftelement.cpp $(IntermediateDirectory)/pinball_targetleftelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/targetleftelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_targetleftelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_targetleftelement.cpp$(DependSuffix): ../../../src/pinball/targetleftelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_targetleftelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_targetleftelement.cpp$(DependSuffix) -MM "../../../src/pinball/targetleftelement.cpp"

$(IntermediateDirectory)/pinball_targetleftelement.cpp$(PreprocessSuffix): ../../../src/pinball/targetleftelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_targetleftelement.cpp$(PreprocessSuffix) "../../../src/pinball/targetleftelement.cpp"

$(IntermediateDirectory)/pinball_targetrightelement.cpp$(ObjectSuffix): ../../../src/pinball/targetrightelement.cpp $(IntermediateDirectory)/pinball_targetrightelement.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Downloads/Dev/pin/pinball/src/pinball/targetrightelement.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pinball_targetrightelement.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_targetrightelement.cpp$(DependSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_targetrightelement.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_targetrightelement.cpp$(DependSuffix) -MM "../../../src/pinball/targetrightelement.cpp"

$(IntermediateDirectory)/pinball_targetrightelement.cpp$(PreprocessSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_targetrightelement.cpp$(PreprocessSuffix) "../../../src/pinball/targetrightelement.cpp"

$(IntermediateDirectory)/resources.rc$(ObjectSuffix): resources.rc
	$(RcCompilerName) -i "C:/Work/Downloads/Dev/pin/pinball/build/windows/codelite/resources.rc" $(RcCmpOptions)   $(ObjectSwitch)$(IntermediateDirectory)/resources.rc$(ObjectSuffix) $(RcIncludePath)

-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(ConfigurationName)/*$(ObjectSuffix)
	$(RM) $(ConfigurationName)/*$(DependSuffix)
	$(RM) $(OutputFile)
	$(RM) $(OutputFile).exe
	$(RM) ".build-release/pinball"


