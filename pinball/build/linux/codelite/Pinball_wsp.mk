.PHONY: clean All

All:
	@echo ----------Building project:[ pinball - Release_x32 ]----------
	@"$(MAKE)" -f "pinball.mk"
clean:
	@echo ----------Cleaning project:[ pinball - Release_x32 ]----------
	@"$(MAKE)" -f "pinball.mk" clean
