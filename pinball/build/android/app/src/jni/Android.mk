LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := beanFarmer

LOCAL_SRC_FILES := pinball.cpp bumperelement.cpp directionrolloverelement.cpp element.cpp elementgroup.cpp rolloverlightelement.cpp slingshotelement.cpp targetleftelement.cpp targetrightelement.cpp plunger.cpp
LOCAL_STATIC_LIBRARIES := orx

LOCAL_ARM_MODE := arm

include $(BUILD_SHARED_LIBRARY)

$(call import-module,lib/static/android)

