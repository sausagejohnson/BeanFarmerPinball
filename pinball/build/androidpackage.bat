REM ---- android packager will output
REM ---- prepackaged files to build into
REM ---- build/android.
REM ---- Import this structure into eclipse.


REM -- Clear expect files from folders first.

call androidclean.bat

REM -- List and copy data to the assets folder
cd android\app\src\main
IF NOT EXIST assets mkdir assets

cd ..
cd ..
cd ..
cd ..
cd ..
cd data
cd
REM pause
for /D %%f in (.\*) do copy "%%f\*" "..\build\android\app\src\main\assets\" 
REM pause

REM -- Concat all located ini files into the assets folder
cd ..
cd bin
copy *.ini ..\build\android\app\src\main\assets\orxd.ini
copy ..\build\android\app\src\main\assets\orxd.ini ..\build\android\app\src\main\assets\orx.ini

REM -- Copy source files to the jni folder
cd ..
cd src
for /D %%f in (.\*) do copy "%%f\*.cpp" "..\build\android\app\src\jni\"
for /D %%f in (.\*) do copy "%%f\*.h" "..\build\android\app\src\jni\"

cd ..
cd build
pause