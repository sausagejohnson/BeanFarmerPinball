# README #

Thanks for dropping by the development repository for Bean Farmer Pinball, part of an upcoming Hip Pocket Pinball series.

### What is this repository for? ###

* Anyone wanting to download a version for their desktop or mobile.
* Source is available if you want to tinker around with Orx.
* Versions available for Windows, Linux, Mac OSX and Android. IOS in not planned.

### How do I get set up? ###

* For Android, download the .apk file and install it. It doesn't require any permissions as yet. You will need to enable "Unknown Sources" to play it, but your device will prompt you when you try to install it.
* For Desktop, installers are available on Windows and Mac. For linux, unzip the archive and run the pinball executable.

### Controls ###
* For desktop, the keys to play are shown on the title screens. You can pull back the plunger, or whack the plunger forward to launch the ball. 
* For mobile, swipe down to vary the plunger and let go to launch. Flipper touchzones are in the bottom left and right of the screen.

### How to play ###
Press Enter to Start the game on Desktop. Swipe on Android.

Hold the down arrow to pull back the plunger, or press the up arrow to smack the plunger hard. On Android, simple swipe and hold to pull back and release.

Press the flippers with left and right shift, or left and right control keys. On Android, touch to bottom left and right corner regions on your device.

Light the top lights, left lights or right lights to increase the bean pole multiplier and to close the safety traps. The traps will stay closed for 10 seconds x the multiplier, up to 60 seconds.

If all light groups are lit, multiball will begin.

Press the right flipper key to move the light positions at the top to the right.

Every element that scores points is worth more with the bean pole multiplier. Bumpers worth 1000 are worth 6000 with a 6x multiplier.

Extra balls are awarded 500,000, 1,000,000, 2,000,000, 3,000,000, 5,000,000 and 10,000,000. 

### Contribution guidelines ###

* I would love your feedback on the game: sausage@zeta.org.au. And please tell your friends. 
* You're welcome to dig through code to help learn about the Orx Portable Game Engine.

### Who do I talk to? ###

* Wayne sausage@zeta.org.au
* Or catch me on the forums at: http://www.orx-project.org/forum